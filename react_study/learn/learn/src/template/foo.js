function Foo(props) {
	console.log(props.name)
	const { name, fn, sex } = props
	const toF = (age) => {
		fn(age)
	}
	return (
		<div className="Foo" xx={toF(18)}>
			<p> hello {name}</p>
            <p>{sex}</p>
		</div>
	)
}

export default Foo
