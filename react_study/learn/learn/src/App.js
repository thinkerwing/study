import logo from './logo.svg'
import { Button } from 'antd'
import './App.css'
import './template/foo'
import Foo from './template/foo'
import Home from './page/home2'
import Test from './page/test'
function App() {
	const fn = (age) => {
		console.log(age)
	}
	return (
		<div className="App">
			<Foo name="zw" sex="boy" fn={fn}></Foo>
			<Button type="primary">Button</Button>
			<Home />
            <Test />
		</div>
	)
}

export default App
