import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import store from '../store'
import { addAction, incAction } from '../store/actionCreators'
import { connect } from '../utils/connect'

function Home(props) {
	return (
		<div>
			<h1>home2</h1>
			<h2>当前计数: {props.counter}</h2>
			<button onClick={(e) => props.increment()}>+1</button>
			<button onClick={(e) => props.addAction(5)}>+5</button>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		counter: state.counter,
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		increment: function () {
			dispatch(incAction())
		},
		addAction: function (num) {
			dispatch(addAction(num))
		},
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)

// connect(参数1, 参数2)(home) 高阶组件调用函数 返回我们当前使用的组件
