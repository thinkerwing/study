// 一般写业务逻辑
import store from './store/index.js'

import {
	addAction,
	subAction,
	incAction,
	decAction,
} from './store/actionCreators.js'
// 订阅store的修改
store.subscribe(() => {
	console.log('state 发生了改变', store.getState())
})

// actions

store.dispatch(addAction(10));
store.dispatch(addAction(15));
store.dispatch(subAction(8));
store.dispatch(subAction(5));
store.dispatch(incAction());
store.dispatch(decAction());


