let sum: (x: number, y: number) => number
const result = sum(1,2)
type PlusType  = (x: number, y: number) => number
let sum2: PlusType
const result2 = sum2(2,3)
type StrOrNumber  = boolean | number
// let result3: StrOrNumber = 's'  
let result3: StrOrNumber = true
result3 = 123

// const str: 'name' = 'name2'
// const number: 1 = 2

type Direction = 'up' | 'down' | 'left' | 'right'
let qunar: Direction = 'left'
// let qunar2: Direction = 'LEFT'

interface IName {
    name: string
}
type IPerson = IName & { age: number }
let person: IPerson = { name: '123', age: 123}
// 都是为了实现对象形式的组合和扩展，type作为类型别名使用交叉组合
//接口可以在 extends 或 implements 子句中命名，但对象类型文字的类型别名不能。
//一个接口可以有多个合并的声明，但对象类型文字的类型别名不能。