// global object 
const a: Array<number> = [1, 2, 3]
const date = new Date()
date.getTime()
const reg = /abc/
reg.test('abc')

//build-in object
Math.pow(2, 2)
//Dom and Bom
let body = document.body //let body: HTMLElement
let allLis = document.querySelectorAll('li')
allLis.keys()

document.addEventListener('click', (e) => { // MouseEvent
    e.preventDefault()
})

//Utility Types 功能性 帮助性 
interface IPerson {
    name: string
    age: number
}
let thinker: IPerson = { name:'thinker', age:20 }
type IPartial = Partial<IPerson>
let thinker2: IPartial = { name:'thinker' }
type IOmit = Omit<IPerson, 'name'>
let thinker3: IOmit = { age:20 }