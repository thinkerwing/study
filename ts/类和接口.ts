//
// interface仿佛是一种契约，定义约束所有内容
interface Radio {
    switchRadio(trigger: boolean): void
}
interface Batterry {
    checkBatteryStatus(): void
}
interface RadioWithBattery extends Radio {
    checkBatteryStatus(): void
}
class Car implements Radio {
    switchRadio(trigger: boolean) {

    }
}
class Cellphone implements Radio,Batterry{
    switchRadio(trigger: boolean) {

    }
    checkBatteryStatus() {

    }
}