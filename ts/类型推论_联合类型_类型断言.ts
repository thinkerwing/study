let str = 'str'
// str = 123 //不能将类型“number”分配给类型“string”。

// union types
let numberOrString : number | string
// numberOrString.toString() // 只能访问共有类型

// 类型断言 union type 告诉编译器你比他更了解这个类型
function getLength(input: string | number) : number {
    const str = input as string
    if (str.length) {
        return str.length
    }  else {
        const number = input as number
        return number.toString().length
    }
}

// type guard
function getLength2(input: string | number) : number {
    const str = input as string
    if (typeof input === 'string') {
        return input.length
    } else {
        return input.toString().length
    }
}
