function add(x: number, y: number, z?: number): number {
    if (typeof z === 'number') {
        return x + y + z
    } else {
        return x + y
    }
}
    // add(1, '2') //类型“string”的参数不能赋给类型“number”的参数
    interface ISum {
        (x: number, y: number, z?: number): number
    }
    
    let add2: ISum = add
    // ts 中冒号后面都是在声明类型 和实际代码逻辑没有关系