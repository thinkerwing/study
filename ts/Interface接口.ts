interface Person {
    readonly id: number
    name: string
    age: number
}
let thinker: Person = {
    id: 1,
    name: 'thinker',
    age: 20
}
// thinker.id = 1 //报错 无法分配到id 因为是只读属性
