// enums 常量是指不会被改变的值 有些取值是在一系列内的：一周内的七天 红黄蓝 四个方位
// npm install -g ts-node -> ts-node 枚举.ts
// 只有常量值可以常量枚举 计算值不能枚举
const enum Direction {
    Up = 'UP',
    Down = 'DOWN',
    Left = 'LEFT',
    Right = 'RIGHT'
}
console.log(Direction.Up);
console.log(Direction.Up[0]);
const value = 'UP'
if (value === Direction.Up) {
    console.log('GO UP')
}