// 类(class) 定义了一切事物的抽象特点 比如蓝图 造车的图纸
// 对象(object) 类的实例  奥迪就是车的实例
// 面向对象(oop) 三大特性：封装、继承、多态
// 封装将数据操作细节隐藏起来，只暴露接口，外接只能通过接口访问对象
// 子类继承父类的所有特征、还有一些特性
// 对一个方法有不同的响应  猫和狗都有吃的属性，吃的不一样  
// public:修饰的属性或方法是共有的
// private:修饰的属性或方法是私有的
// protected:修饰的属性或方法是受保护的
class Animal {
    name: string
    constructor(name) {
        this.name = name
    }
    protected run() {
        return `${this.name} is running`
    }
}
const snake = new Animal('lily')
// console.log(snake.run()); //属性“run”受保护，只能在类“Animal”及其子类中访问。
class Dog extends Animal {
    bark() {
        return `${this.name} is barking`
    }
}
const xiaobao = new Dog('xiaobao')
console.log(xiaobao.bark())
// console.log(xiaobao.run()) //属性“run”受保护，只能在类“Animal”及其子类中访问。
class Cat extends Animal {
    static categories = ['mamal']
    constructor(name) {
        super(name)
        console.log(this.name)    
    }
    run() {
        return 'Meow, ' + super.run()
    }
}
const maomao = new Cat('maomao')
console.log(maomao.run())
console.log(Cat.categories);




