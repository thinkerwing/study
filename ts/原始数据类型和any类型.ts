let isDone: boolean = false
let age: number = 123
let firstName: string = 'thinkerwing'
let message: string = 'Hello ${firstName}'
let u: undefined = undefined
let n:null = null
let num: number = undefined
let notSure: any = 4
notSure = 'maybe string'
notSure = true
notSure.myName
notSure.getName()

