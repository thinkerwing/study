// interface仿佛是一种契约，定义约束所有内容
// interface 和 type 的区别
/*
如果是定义非对象类型，通常推荐使用type，比如Direction、Alignment、一些Function
 */
interface ISwim {
	swimming: () => void
}

interface IEat {
	eating: () => void
}

// 类实现接口
class Animal {}

// 继承：只能实现单继承
// 实现： 实现接口， 类可以实现多个接口
// implements和extends 前者通过声明这个类实现一个或者多个接口，
// 而后者通过继承父类，拥有了父类的功能，可以重写父类的方法，也可以不重写。相对于implements而言，它是实现接口，可想而知，实现接口（interface）一般为空的，所以你一般要重写接口所有的方法。一个类不能extends多个类，但是可以通过接口然后让一个类去implements多个接口
// punlic class A extends B implements C,D,E 这样。
class Fish extends Animal implements ISwim, IEat {
	swimming() {
		console.log('Fish swimming')
	}
	eating() {
		console.log('Fish eating')
	}
}

class Person implements ISwim {
	swimming() {
		console.log('person swimming')
	}
}
// 编写一些公共api： 面向接口编程
function swimAction(swimable: ISwim) {
    swimable.swimming()
}
function eatAction(eatable: IEat) {
    eatable.eating()
}

// 1. 所有实现了接口的类对应的对象，都是可以传入
swimAction(new Fish())
eatAction(new Fish())
swimAction(new Person())
swimAction({swimming: function() {}})
