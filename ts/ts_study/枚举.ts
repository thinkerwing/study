/*
通俗来说，枚举就是一个对象的所有可能取值的集合
枚举其实就是将一组可能实现的值，一个个列举出来，定义在一个类型中，这个类型就是枚举类型
枚举允许开发者定义一组命名常量，常量可以是数组、字符串类型

字符串枚举
字符串枚举的概念很简单，但是有细微的 运行时的差别。 在一个字符串枚举里，
每个成员都必须用字符串字面量，或另外一个字符串枚举成员进行初始化。
*/

enum DirectionNum { //数字枚举
    Up = 1,
    Down,
    Left,
    Right
}
enum BooleanLikeHeterogeneousEnum { //异构枚举 异构枚举的成员值是数字和字符串的混合：
    No = 0,
    Yes = "YES",
}
// 大多数情况下，枚举是十分有效的方案。 然而在某些情况下需求很严格。 
// 为了避免在额外生成的代码上的开销和额外的非直接的对枚举成员的访问，
// 我们可以使用 const枚举。 常量枚举通过在枚举上使用 const修饰符来定义。
const enum Enum {
    A = 1,
    B = A * 2
}
let arr = [Enum.A, Enum.B]
console.log(arr);
enum Direction { // 字符串枚举
	LEFT = 'LEFT',
	RIGHT = 'RIGHT',
	TOP = 'TOP',
	BOTTOM = 'BOTTOM',
}

let name: string = 'abc'
let d: Direction = Direction.BOTTOM

function turnDirection(direction: Direction) {
	console.log(direction)
	switch (direction) {
		case Direction.LEFT:
			console.log('改变角色的方向向左')
			break
		case Direction.RIGHT:
			console.log('改变角色的方向向右')
			break
		case Direction.TOP:
			console.log('改变角色的方向向上')
			break
		case Direction.BOTTOM:
			console.log('改变角色的方向向下')
			break
		default:
			/**
             never，never表示永远不存在的类型。比如一个函数总是抛出错误，而没有返回值。或者一个函数内部有死循环，
             永远不会有返回值。函数的返回值就是never类型。
             void, 没有显示的返回值的函数返回值为void类型。如果一个变量为void类型，只能赋予undefined或者null。
             */
			const foo: never = direction
			break
	}
}

turnDirection(Direction.LEFT)
turnDirection(Direction.RIGHT)
turnDirection(Direction.TOP)
turnDirection(Direction.BOTTOM)

export {}
/**
  应用场景
// 枚举
enum LoginAPI {
  AccountLogin = '/signIn', // 登录
  AccountRegist = '/signUp', // 注册
  LoginUserInfo = '/users/', //user/id
  UserMenus = '/role/' //用法：role/id/menu
}

export function accountLoginRequest(account: IAccount) {
  return lxRequest.post<IDataType<ILoginResult>>({
    url: LoginAPI.AccountLogin,
    data: account
  })
}
 */
