interface IPerson {
	name: string
	age: number
	height: number
}
// const p: IPerson = {
//     name: 'why',
//     age: 18,
//     height: 1.88,
//     address: 'fit'   
// }
const info = {
    name: 'why',
    age: 18,
    height: 1.88,
    address: 'fit'   
}
// 赋值对象的引用 freshness 擦除
// 当我们把info赋值到这里的时候，类型检测的时候没有的类型会擦除，但是不能少
const p: IPerson = info
console.log(p);
console.log(info);


