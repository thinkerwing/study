interface IFoo {
	name: string
}
interface IFoo {
	age: number
}
const foo: IFoo = {
	name: 'why',
	age: 18,
}

// type 定义的别名不能重复
// interface 可以重复合并