interface ILength {
	length: number
}


function getLength<T extends ILength>(arg: T) {
	return arg.length
}
function getLength2<T>(arg: string) {
	return arg.length
}

getLength('abc')
getLength(['abc', 'cba'])
getLength({ length: 100 })
console.log(getLength('abc'))
console.log(getLength({ length: 100 }));

console.log(getLength2<string>('abcd'))

