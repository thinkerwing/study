interface IPerson<T1 = string, T2 = number> {
	name: T1
	age: T2
}

const p: IPerson = {
	name: 'why',
	age: 18,
}
console.log(p);

interface ConfigFn{
    <T>(value:T):T;
}
 
const getData:ConfigFn=function<T>(value:T):T{
    return value;
}
console.log(getData<string>('张三'));
