let  arrOfNumbers: number[] = [1, 2, 3]
arrOfNumbers.push(3)

// 类数组
function test() {
    console.log(arguments) //会有提示Iarguments 
    // arguments.forEach //没有数组的方法
    // let arr: any[] = arguments  //而且不能复制给数组
}
// 数组中元素的数据类型都一般是相同的（any[] 类型的数组可以不同）
// 如果存储的元素数据类型不同，则需要使用元组。
// 元组中允许存储不同类型的元素，元组可以作为参数传递给函数。
let user: [string, number] = ['thinker', 20]
// user.push(true) //报错 因为只能是string或number
