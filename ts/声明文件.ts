// 很多第三方库不是ts写的 直接使用会报错
// 1.
declare var jQuery: (selector: string) => any
jQuery('#foo')

// 2. 
// npm install --save-dev @types/jquery
// https://www.typescriptlang.org/dt/search?search=