function fn1() {
    console.log('fn1',this);
}
fn1() // window
const a = {
    name : 'a',
    say() {
        // this 当前对象
        console.log('say',this); 
    },
    wait() {
        setTimeout(function () {
            // this === window
            console.log('wait',this);
            // setTimeout 触发的，不是作为对象的方法
        })
    }
}
const b = {
    name : 'b',
    say() {
        // this 当前对象
        console.log('b-say',this); 
    },
    wait() {
        setTimeout( () => {
            // this 当前对象
            console.log('b-wait',this);
            // 箭头函数的this永远是取他作用域的this
        })
    }
}
a.say()
a.wait()
b.say()
b.wait()