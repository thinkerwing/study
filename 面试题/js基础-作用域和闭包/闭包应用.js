/** 实际开发中闭包的应用
 * 隐藏数据
 * 如做一个简单的cache工具
 * */
function createCache() {
    const data = {} // 闭包中的数据，被隐藏，不被外界访问
    return  {
        set: function (key, val) {
            data[key] = val
        },
        get: function (key, val) {
            return data[key]
        }
    }
}
// data.b = 200 // data is not defined
const c = createCache()
c.set('a', 100)
console.log(c.get('a'));