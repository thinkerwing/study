let i, a
for (i = 0; i < 10; i++) {
    a = document.createElement('a')
    a.innerHTML = i + '<br>'
    a.addEventListener('click', function (e) {
        e.preventDefault()
        alert(i)
    })
    document.body.appendChild(a)
}
// 全局作用域 每次点击都是10
for ( let j = 0; j < 10; j ++) {
    a = document.createElement('a')
    a.innerHTML = j  + '<br>'
    a.addEventListener('click', function (e) {
        e.preventDefault()
        alert(j)
    })
    document.body.appendChild(a)
}
// for 块级作用域 考虑到每一个块
for ( var k = 0; k < 10; k ++) {
    a = document.createElement('a')
    a.innerHTML = k  + '<br>'
    a.addEventListener('click', function (e) {
        e.preventDefault()
        alert(k)
    })
    document.body.appendChild(a)
}