// 值类型的变量直接存储数据，而引用类型的变量持有的是数据的引用，数据存储在数据堆中。
// 值类型
let a = 100
let b = a
a = 200 
console.log(b); // 100

// 引用类型
let a2 = { age: 20 }
let b2 = a2 
b2.age = 21
console.log(a2.age); // 21

// 常见值类型 
let c // undefined
const d = 'abc'
const e = 100
const f = true
const g = Symbol('g')
console.log(typeof(c),typeof(d),typeof(e),typeof(f),typeof(g));

// 常见引用类型
const obj = { x: 100 }
const arr = ['a', 'b', 'c']
const n = null // 特殊引用类型，指针指向空地址
// 特殊引用类型，但不用于存储数据，所以没有“拷贝、复制函数”
function fn() {}
// 判断函数
console.log(typeof console.log);
console.log( function(){});
// 能识别引用类型（不能在继续识别）
console.log(typeof null);
console.log( typeof ['a', 'b']);
console.log(typeof {x: 100});
