// 字符串拼接
const a = 100 +10
const b = 100 + '10'
const b2 = 100 + parseInt('10')
const c = true + '10'
console.log(a, b, b2, c);
// ==运算符
console.log(100 == '100');
console.log(0 == '');
console.log(0 == false);
console.log(false == '');
console.log(null == undefined);
// 除了 == null 之外，其他一律都用 === 
const obj = { x: 100}
if (obj.a == null) {}
//相当于
// if (obj.a === null || obj.a === undefined) {}
console.log(!!0 === false, 'falsely');
console.log(!!NaN === false, 'falsely');
console.log(!!'' === false, 'falsely');
console.log(!!null === false, 'falsely');
console.log(!!undefined === false, 'falsely');
console.log(!!false === false, 'falsely');
const aaa = true
if (aaa) {
    console.log('aaa');
}
// 逻辑判断
console.log(10 && 0);
console.log('' || 'abc');
let abc
console.log(!abc);