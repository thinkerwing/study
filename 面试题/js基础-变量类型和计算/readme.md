*变量类型
值类型 vs 引用类型
typeof 运算符
识别所有值类型 识别函数 判断是否是引用类型
深拷贝(引用类型的复制是深拷贝 =赋值是浅拷贝)
*变量计算
字符串拼接
==
// 除了 == null 之外，其他一律都用 === 
const obj = { x: 100}
if (obj.a == null) {}
//相当于
// if (obj.a === null || obj.a === undefined) {}
if语句和逻辑运算
truly 变量：!!a === true的变量
falsely变量：!!a === false的变量

值类型和引用类型的区别
const obj1 = { x:100, y:200}
const obj2 = obj1
let x1 = obj1.x // 值类型复制x：100
obj2.x = 101
x1 = 102
console.log(obj1) // {x:101}

手写深拷贝
注意判断值类型和引用类型
注意判断是数组还是对象
递归


小结
值类型vs引用类型，堆栈类型，深拷贝
typeof 运算符
类型转换，truly和falsely变量