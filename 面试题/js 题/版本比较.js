/**
 * @param {string} version1
 * @param {string} version2
 * @return {number}
 */
 var compareVersion = function(version1, version2) {
    v1 = version1.split('.')
    v2 = version2.split('.')
    let n = Math.max(v1.length, v2.length)
    for (let i = 0; i < n; i++) {
        let code1 = (v1[i] == undefined) ? 0 : parseInt(v1[i])
        let code2 = (v2[i] == undefined) ? 0 : parseInt(v2[i])
        if (code1 > code2) { return 1 }
        else if (code1 < code2) { return -1 }
    }
    return 0
};
let res = compareVersion(version1 = "1.01", version2 = "1.001")
let res2 = compareVersion(version1 = "1.0", version2 = "1.0.0")
let res3 = compareVersion(version1 = "0.1", version2 = "1.1")
let res4 = compareVersion(version1 = "1.0.1", version2 = "1")
let res5 = compareVersion(version1 = "7.5.2.4", version2 = "7.5.3")
console.log(res, res2, res3, res4, res5);