// 找数组中重复次数最多的数字
let arr = [9, 1, 6, 3, 3, 1, 2, 1, 8]
let map = new Map()
for (let i = 0; i < arr.length; i++) {
    let c = arr[i]
    map.set(c, map.has(c) ? map.get(c) + 1 : 1)
}
console.log(map);
let res = Array.from(map).sort((a,b) => a[1] - b[1]).pop()[0]
console.log(res);