/**
 * 如何准确判断一个变量是不是数组
 * 手写一个简易的jQuery，考虑插件和拓展性
 * class的原型本质，怎么理解
 **/ 
let a = {}
console.log(a instanceof Object)
let b = []
// 1 instanceof
console.log(b instanceof Array)
// 2 数组方法 isArray()
console.log(Array.isArray(a))
console.log(Array.isArray(b))
// 3 原型prototype + toString +  call
console.log(Object.prototype.toString.call(b).indexOf('Array') !== -1);
// 4 原型prototype + isPrototypeOf()方法
console.log(Array.prototype.isPrototypeOf(b))
// 5 构造函数 constructor
console.log('构造函数 constructor', b.constructor.toString().indexOf("Array") !== -1);

class jQuery {
    constructor(selector) {
        const result = document.querySelectorAll(selector)
        const length = result.length
        for (let i = 0; i < length; i++) {
            this[i] = result[i]
        }
        this.length = length
        this.selector = selector
    }
    get(index) {
        return this[index]
    }
    each(fn) {
        for (let i = 0; i < this.length; i++) {
            const elem = this[i]
            fn(elem)
        }
    }
    on(type, fn) {
        return this.each(elem => {
            elem.addEventListener(type, fn, false)
        })
    }
    // 拓展很多 DOM API
}
// 插件
jQuery.prototype.dialog = function (info) {
    alert(info)
}
// 复写 造轮子 
class myJQuery extends jQuery {
    constructor(selector) {
        super(selector)
    }
    // 拓展自己的方法
    addClass(className) {
        console.log('add');
    }
}
// const $p = new jQuery('p')
// const $p.get(1)
// const $p.each((elem) => {console.log(elem.nodeName)})
// $p.on('click', () => alert('click'))
/**
 * 其实在 JS 中并不存在类，class 只是语法糖，本质还是函数。
 * class 实现继承的核心在于使用 extends 表明继承自哪个父类，
 * 并且在子类构造函数中必须调用 super，因为这段代码可以看成 Parent.call(this, value)。
 */