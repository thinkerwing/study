```
let arr = ['a', 'b', 'c']
arr[3] = 'd'
arr["key"] = 'e'

for (let key in arr) {
    console.log(key);
}

console.log("======");

for (let value of arr) {
    console.log(value)
}

console.log("======");

let keys = Object.keys(arr);

console.log(keys);
// 0
// 1
// 2
// 3
// key
// ======
// a
// b
// c
// d
// ======
// [ '0', '1', '2', '3', 'key' ]
```
# for in和Object.keys遍历对象有什么区别？
两者之间最主要的区别就是Object.keys( )不会走原型链，而for in 会走原型链
```
Object.prototype.test = 'test';
var obj= {
    a:1,
    b:2,
}
console.log(Object.keys(obj))
// ["a", "b"]      Object.keys不会输出原型链中的数据；
for(var key in obj){
    console.log(key)
}
// a
// b
// test　　　　//for in 会把原型链中test 输出

```
# for of为什么不能遍历Object对象？

因为for of遍历依靠的是遍历器Iterator。
而 Array（数组）, String（字符串）, Map（映射）, Set（集合）,TypedArray(类型化数组)、arguments、NodeList对象、Generator等可迭代的数据结构等早就内置好了Iterator（迭代器），它们的原型中都有一个Symbol.iterator方法，而Object对象并没有实现这个接口，使得它无法被for...of遍历。

# object.keys枚举的顺序
数字类型先排序  不是数字的按照创建顺序 小数也比较特殊 感觉没归在数字类型