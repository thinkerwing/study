const bubbleSort = (arr) => {
	const len = arr.length - 1
	// i为轮数 因i从0开始 即i<len-1
	for (let i = 0; i < len; i++) {
		// j为比较次数 第i轮仅需比较length-1-i次
		for (let j = 0; j < len - i; j++) {
			// 通过判断相邻两项的大小 决定是否交换位置
			if (arr[j] > arr[j + 1]) {
				let temp = arr[j]
				arr[j] = arr[j + 1]
				arr[j + 1] = temp
			}
		}
	}
	return arr
}
let arr = [5, 3, 2, 4, 1]
let res = bubbleSort(arr)
console.log(res);