class EventEmitter {
	constructor() {
		this.list = []
	}
	on(name, fn) {
		this.list.push({ name, fn })
	}
	emit(name, ...args) {
		for (let index in this.list) {
			if (this.list[index].name == name) {
				this.list[index].fn(...args)
			}
		}
	}
	off(name) {
		//删除订阅者
		for (let index in this.list) {
			if (this.list[index].name === name) {
				this.list.splice(index, 1)
			}
		}
	}
}

let p = new EventEmitter()

p.on('zw', (v1, v2) => {
	console.log(v1, v2)
})
p.on('wxk', (v1) => {
	console.log(v1)
})

setTimeout(() => {
	p.emit('zw', 'yyds', 'nb')
	p.emit('wxk', {
		name: 'lx',
		age: 18,
	})
}, 2000)
// p.off('zw')