### 1.OptionsAPI更换成CompositionAPI

- OptionsAPI当组件代码很多时，如果增加需求，要在 data、methods、computed 以及 mounted 中反复的跳转
- CompositionAPI将零散分布的逻辑组合在一起来维护，并且还可以将单独的功能逻辑拆分成单独的文件

#### 1.1.setup函数

- setup 是 Vue3.x 新增的一个选项， 他是组件内使用 `Composition API`的入口
- setup 执行时机是在 beforeCreate 之前执行
- **setup中不能访问this**
- setup函数接收两个参数
  - props：响应式的
  - context：this中最常用的三个属性值，可解构成{attrs, slot, emit}

#### 1.2.响应式APIreactive、ref 与 toRefs

- 这些方法都需要从vue中导入
- 在 **vue2.x** 中， 定义数据都是在`data`中， 但是 **Vue3.x** 可以使用`reactive`和`ref`来进行数据定义
- reactive：只能处理复杂对象类型的双向绑定
- ref：既能处理对象类型的双向绑定，也能处理基本类型的双向绑定，进行操作时需要` .value`才可以
  - ref返回的响应式对象是只包含一个名为value参数的RefImpl对象，在js中获取和修改都是通过它的value属性；
  - 但是在模板中被渲染时，自动展开内部的值，因此不需要在模板中追加`.value`
- toRefs ：将一个 reactive 对象转化为属性全部为 ref 对象的普通对象

```vue
<template>
  <div class="homePage">
    <p>第 {{ year }} 年</p>
    <p>姓名： {{ nickname }}</p>
    <p>年龄： {{ age }}</p>
  </div>
</template>

<script>
import { defineComponent, reactive, ref, toRefs } from "vue";
export default defineComponent({
  setup() {
    const year = ref(0);
    const user = reactive({ nickname: "xiaofan", age: 26, gender: "女" });
    setInterval(() => {
      year.value++;
      user.age++;
    }, 1000);
    return {
      year,
      // 使用reRefs
      ...toRefs(user),
    };
  },
});
</script>

```

#### 1.3.生命周期的变化

- 使用生命周期都需要从vue中导入，需要加上“on”
- setup在beforeCreate之前执行
- ![在这里插入图片描述](https://img-blog.csdnimg.cn/bb2f13f180bd4ef385ec39c247d8a687.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAVGhpbmtlcldpbmc=,size_13,color_FFFFFF,t_70,g_se,x_16)


#### 1.4.响应式侦听watch 与 watchEffect 的用法

```
watch(source, callback, [options])
```

- source: 可以支持 string,Object,Function,Array; 用于指定要侦听的响应式变量
- callback: 执行的回调函数
- options：支持 deep、immediate 和 flush 选项。
- watchEffect不需要手动传入依赖
- 每次初始化时watchEffect都会执行一次回调函数来自动获取依赖
- watchEffect无法获取到原值，只能得到变化后的值

```js
import { defineComponent, ref, reactive, toRefs, watch } from "vue";
export default defineComponent({
  setup() {
    //侦听 reactive 定义的数据
    const state = reactive({ nickname: "Kylin", age: 20 });
		let year = ref(0)
    
    setTimeout(() => {
      state.age++;
      year.value++
    }, 1000);

    // 修改age值时会触发 watch的回调
    watch(
      () => state.age,
      (newAge, oldAge) => {
        console.log("新值:", newAge, "老值:", oAge);
      }
    );
    
    watchEffect(() => {
        console.log(state);
        console.log(year);
      }
    );
    
    //侦听 ref 定义的数据
    const year = ref(0);

    setTimeout(() => {
      year.value++;
    }, 1000);

    watch(year, (newVal, oldVal) => {
      console.log("新值:", newVal, "老值:", oldVal);
    });


    return {
      ...toRefs(state),
    };
  },
});

```

### 2.Vue3可以自定义hooks

- 约定这些「自定义 Hook」以 use 作为前缀，和普通的函数加以区分

### 3.简单对比vue2.x 与 vue3.x 响应式

- `Object.defineProperty`只能劫持对象的属性， 而 Proxy 是直接代理对象
  - `Object.defineProperty`只能劫持对象属性，需要遍历对象的每一个属性，如果属性值也是对象，就需要递归进行深度遍历。
  - Proxy 直接代理对象， 不需要遍历操作
- `Object.defineProperty`只对初始对象里的属性有监听作用，对新增属性需要手动进行`Observe`
  - `Object.defineProperty`劫持的是对象的属性，所以新增属性时，需要重新遍历对象， 对其新增属性再次使用`Object.defineProperty`进行劫持。
  - 而Proxy不仅对初始对象的属性有监听作用，对新增的也有

### slot插槽用法变更

- 在 Vue2.x 中具名插槽和作用域插槽分别使用`slot`和`slot-scope`来实现， 
- 在 Vue3.0 中将`slot`和`slot-scope`进行了合并统一使用

```js
// 子组件
<slot name="content" :data="data"></slot>
export default {
    data(){
        return{
            data:["走过来人来人往","不喜欢也得欣赏","陪伴是最长情的告白"]
        }
    }
}
//Vue2
<!-- 父组件中使用 -->
<template slot="content" slot-scope="scoped">
    <div v-for="item in scoped.data">{{item}}</div>
<template>

//Vue3
<!-- 父组件中使用 -->
 <template v-slot:content="scoped">
   <div v-for="item in scoped.data">{{item}}</div>
</template>

<!-- 也可以简写成： -->
<template #content="{data}">
    <div v-for="item in data">{{item}}</div>
</template>

```

### v-model升级

- 同一组件可以同时设置多个 `v-model`
- 在自定义组件上使用`v-model`, 相当于传递一个`modelValue` 属性， 同时触发一个`update:modelValue`事件

```
<modal v-model="isVisible"></modal>
<!-- 相当于 -->
<modal :modelValue="isVisible" @update:modelValue="isVisible = $event"></modal>
```

- 如果要绑定属性名， 只需要给`v-model`传递一个参数就行, 同时可以绑定多个`v-model`：

```
<modal v-model:visible="isVisible" v-model:content="content"></modal>

<!-- 相当于 -->
<modal
    :visible="isVisible"
    :content="content"
    @update:visible="isVisible"
    @update:content="content"
/>
```

### 新增组件

- Teleport/ˈtelɪpɔːt/
  - 传送：可以将插槽中的元素或者组件传送到页面的其他位置：
- Suspense /səˈspens/
  - 它允许我们的程序在等待异步组件时渲染一些后备的内容，可以让我们创建一个平滑的用户体验

- Fragment
  - Vue2的template中需要根元素，在Vue3中不需要了

### 自定义指令

在 Vue 3 中对自定义指令的 API 进行了更加语义化的修改， 就如组件生命周期变更一样， 都是为了更好的语义化， 变更如下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/09267300a0cb4829b2914ab4869799bc.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAVGhpbmtlcldpbmc=,size_18,color_FFFFFF,t_70,g_se,x_16)

```js
const { createApp } from "vue"

const app = createApp({})
app.directive('focus', {
    mounted(el) {
        el.focus()
    }
})
```

# 响应式原理区别
Vue2.x中响应式是如何实现的？
vue2.x 响应式是通过 数据劫持 结合 发布订阅模式的方式来实现的， 

采用数据劫持结合发布-订阅模式，通过 Object.defineproperty来劫持各个属性的 setter,getter，当数据变动时，发布消息给订阅者，会触发响应的监听回调。
响应式实现步骤

实现一个监听器 Observer：利用 Object.defineproperty 劫持(监听)各个属性的setter 和 getter。数据变动时，就能监听到数据变化。

实现一个解析器 Compile：解析 Vue 模板指令，将模板中的变量都转成数据，然后初始化渲染页面，并给每个指令对应的节点绑定更新函数；添加监听数据的订阅者，一旦数据有变动，调用更新函数进行数据更新。

实现一个订阅者 Watcher：订阅者是 Observer 和 Compile 之间通信的桥梁 ，负责订阅数据的变化，当数据发生变化时，触发解析器 Compile 中对应的更新函数。

实现一个发布者Dep：采用发布-订阅模式，来收集订阅者 Watcher，对监听器 Observer 和 订 阅者 Watcher 进行统一管理。

在Vue3中针对所有的被监听的对象，存在一张关系表targetMap,key为target，value为另一张关系表depsMap。
depsMap的key为target的每个key，value为由effect函数传入的参数的Set集。

**vue2响应式痛点**
- 递归，消耗大

- 新增/删除属性，需要额外实现单独的API

- 数组，需要额外实现

- Map Set Class等数据类型，无法响应式

- 修改语法有限制

Proxy可以在目标对象上加一层拦截/代理，外界对目标对象的操作，都会经过这层拦截

相比 Object.defineProperty ，Proxy支持的对象操作十分全面：
get、set、has、deleteProperty、ownKeys、defineProperty......等等

get的官方解释：get语法将对象属性绑定到查询该属性时将被调用的函数。

翻译：get在对象里定义了一个查找对象属性的方法，这个方法是一个‘自执行方法’或者说是‘被调用的方法’，使用的时候无需再调用。

与对象方法的区别：对象方法需要调用，而get方法不需要，因为get绑定的是‘方法的调用’

同理set方法

官方解释：当尝试设置属性时，set语法将对象属性绑定到要调用的函数。

解释：set在对象里定义了一个设置对象属性的方法，这个方法是一个‘自执行方法’或者说是‘被调用的方法’，使用的时候无需再调用，而自动执行，以方便直接赋值。

effect：将回调函数保存起来备用，立即执行一次回调函数触发它里面一些响应数据的getter

track（依赖收集）：getter中调用track，把前面存储的回调函数和当前target,key之间建立映射关系

trigger（派发更新）：setter中调用trigger，把target,key对应的响应函数都执行一遍
![在这里插入图片描述](https://img-blog.csdnimg.cn/7310267ad91842dca51305f7af54ef79.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAVGhpbmtlcldpbmc=,size_20,color_FFFFFF,t_70,g_se,x_16)



```
const isObject = val => typeof val === 'object' && val !== null
//缓存已处理的对象，避免重复代理
//WeakMap 对象是一组键/值对的集合，其中键是弱引用的，必须是对象，而值可以是任意的。
const toProxy = new WeakMap() //形如obj:observed
const toRaw = new WeakMap() //形如observed:obj
function reactive(obj){
	if(!isObject(obj)){
		return obj
	}
	//查找缓存，避免重复代理
	if(toProxy.has(obj)){
		return toProxy.get(obj)
	}
	if(toRaw.has(obj)){
		return obj
	}
	/*
	Proxy两个参数
	target:要使用 Proxy 包装的目标对象（可以是任何类型的对象，包括原生数组，函数，甚至另一个代理）。
	handler:一个通常以函数作为属性的对象，各属性中的函数分别定义了在执行各种操作时代理 p 的行为。 
	*/
	const observed = new Proxy(obj,{
		//Reflect 是一个内置的对象，它提供拦截 JavaScript 操作的方法。这些方法与proxy handlers的方法相同
		get(target,key,receiver){
			const res = Reflect.get(target,key,receiver)
			//依赖收集
			track(target,key)
			//递归处理嵌套对象
			return isObject(res)?reactive(res):res
		},
		set(target,key,value,receiver){
			const res = Reflect.set(target,key,value,receiver)
			//触发响应函数
			trigger(target,key)
			return res
		},
		deleteProperty(target,key){
			const res = Reflect.deleteProperty(target,key)
			return res
		}
	})
	//缓存代理结果
	toProxy.set(obj,observed)
	toRaw.set(observed,obj)
	return observed
}

//保存当前活动响应函数作为getter和effect之间的桥梁
const effectStack = []
//设置响应函数，创建effect函数，执行fn并将其入栈
function effect(fn){
	const rxEffect = function(){
		//捕获可能的异常
		try{
			//入栈，用于后续依赖收集
			effectStack.push(rxEffect)
			//运行fn，触发依赖收集
			return fn()
		}finally{
			//执行结束，出栈
			effectStack.pop()
		}
	}
	//默认执行一次响应函数
	rxEffect()
	//返回响应函数
	return rxEffect
}

//映射关系表
//{target:{key:[fn1,fn2]}}
let targetMap = new WeakMap()
function track(target,key){
	//从栈中取出响应式函数
	const effect = effectStack[effectStack.length - 1]
	if(effect){
		let depsMap = targetMap.get(target)
		if(!depsMap){
			depsMap = new Map()
			targetMap.set(target,depsMap)
		}
		//获取key对应的响应函数集
		let deps = depsMap.get(key)
		if(!deps){
			deps = new Set()
			depsMap.set(key,deps)
		}
		//将响应函数加入到对应集合
		deps.add(effect)
	}
}

//触发target，key对应响应函数
function trigger(target,key){
	//获取依赖表
	const depsMap = targetMap.get(target)
	if(depsMap){
		//获取响应函数集合
		const deps = depsMap.get(key)
		console.log(deps)
		if(deps){
			//执行所有响应函数
			deps.forEach(effect=>{
				effect()
			})
		}
	}
}


//使用
//设置响应函数
const state = reactive({
	foo:"aaa"
})
effect(()=>{
	console.log(state.foo)//aaa   bbb
})
state.foo
state.foo = "bbb"

```
vue2数组不响应的问题

data{
    arr:[1,2,3]
}

arr = [2,2]
object.defineproperty 能监听到数组值的变化，但是vue没有实现
vue是对数组元素进行了监听，而没有对数组本身的变化进行监听
改变arr[0].obj1的某个属性可以被检测到，但是改变arr[0]则不能
defineProperty这个劫持方法非常粗暴，用的是遍历，按照这种方法监听数组下标
es6 的proxy，完美的劫持

由于 JavaScript 的限制，Vue 不能检测以下数组的变动：

（1）当你利用索引直接设置一个数组项时，例如：vm.items[indexOfItem] = newValue

（2）当你修改数组的长度时，例如：vm.items.length = newLength

可以响应式更新的方法：
（1）push：在数组后面添加数据
（2）unshift：在数组前面添加数据
（3）pop：删除数组中最后一个元素
（4）shift：删除数组中第一个元素
（5）splice：删除元素 / 插入元素 / 替换元素
（6）Vue.set
```
语法结构：set（"要修改的对象"，索引值，修改后的值）；

//修改对象，下标，修改后的值
Vue.set(this.list,0,"Vue2.0");
```
vue3.2 新特性
 ```
 1. <script lang="ts" setup>
2. v-bind可以用在SFC<style>标签中绑定响应式数据的值。
3. defineCustomElement
该方法接受和 defineComponent 相同的参数，但是返回一个原生的自定义元素，该元素可以用于任意框架或不基于框架使用。
<my-vue-element></my-vue-element>
import { defineCustomElement } from 'vue'
const MyVueElement = defineCustomElement({
  // 这里是普通的 Vue 组件选项
  props: {},
  emits: {},
  template: `...`,
  // 只用于 defineCustomElement：注入到 shadow root 中的 CSS
  styles: [`/* inlined css */`]
})
// 注册该自定义元素。
// 注册过后，页面上所有的 `<my-vue-element>` 标记会被升级。
customElements.define('my-vue-element', MyVueElement)
// 你也可以用编程的方式初始化这个元素：
// (在注册之后才可以这样做)
document.body.appendChild(
  new MyVueElement({
    // 初始化的 prop (可选)
  })
)
```
