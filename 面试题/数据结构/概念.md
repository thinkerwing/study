# 栈的数据结构 
栈是一种后进先出的数据结构，也就是说最新添加的项最早被移出；它是一种运算受限的线性表，只能在表头/栈顶进行插入和删除操作。

　　栈有栈底和栈顶。

　　向一个栈插入新元素叫入栈（进栈），就是把新元素放入到栈顶的上面，成为新的栈顶；

　　从一个栈删除元素叫出栈，就是把栈顶的元素删除掉，相邻的成为新栈顶；

　　也就是说栈里面的元素的插入和删除操作，只在栈顶进行；
如何设计一个链表 
链表在内存中的存储结构

# 堆的数据结构 
堆是一种特殊的完全二叉树。

所有的节点都大于等于（最大堆）或小于等于（最小堆）它的子节点。
栈区： 

那么js中除了上下文引用类型，别的类型的变量都存在哪里呢？

首先可以确定的是基本类型都存放在栈区，该类型的数据存在该变量门牌号（地址）的房间里（数据值）。所以当你在拷贝一个值给另一个变量时，传递的是一个单纯的值，不会再产生任何指向运算。而当你在声明一个引用类型时，该变量所在的房间里存放的数据实际是一个地址值，该地址指向了内存中的另一块区域——堆区。

堆区：

当引用类型被浅拷贝时，因为所分配的数据空间是同一块，位于堆中的某一个位置。所以浅拷贝的数据值被所引用的引用类型变量所共同使用，一发则同全身。

而被深拷贝的时候，会根据执行重新分配一块新的内存空间，两个栈上的引用类型的变量指向运算后会指向堆中不同的地址空间。这个时候，大家各干各的，雨各自无瓜。

这样，在我们想要深拷贝的场景中，我们对引用类型拷贝时就不能使用直接赋值这种方式了，而是要重新分配一块空间给被拷贝值。

# 队列
https://juejin.cn/post/6936891172547723301

队列是遵循先进先出（FIFO，也就是先进来的先出去的意思）原则的一组有序的项。队列是从尾部添加新元素，并从头部移除元素，最新添加元素必须排在队列的末尾。

enqueue：向队列的尾部添加一个元素。

dequeue：移除队列的第一个元素并且返回该元素。

peek：返回队列中最先添加的元素，也是最先被移除的元素。

isEmpty：校验该队列是否为空队列。

size：返回队列中的元素个数，和数组的length类似。
# 链表在内存中的存储结构

链表是一组节点组成的集合，每个节点都使用一个对象的引用来指向它的后一个节点。指向另一节点的引用讲做链。其中，data中保存着数据，next保存着下一个链表的引用。 data2 跟在 data1 后面，而不是说 data2 是链表中的第二个元素。值得注意的是，我们将链表的尾元素指向了 null 节点，表示链接结束的位置。
https://www.jianshu.com/p/f254ec665e57
# 如何设计一个链表
# 链表和数组的优缺点
数组的增删改查，通过shift、unshift、pop、push
数组的优点

随机访问性比较强，可以通过下标进行快速定位。
查找速度快

数组的缺点

插入和删除的效率低，需要移动其他元素。
会造成内存的浪费，因为内存是连续的，所以在申请数组的时候就必须规定七内存的大小，如果不合适，就会造成内存的浪费。
内存空间要求高，创建一个数组，必须要有足够的连续内存空间。
数组的大小是固定的，在创建数组的时候就已经规定好，不能动态拓展。

链表的优点

插入和删除的效率高，只需要改变指针的指向就可以进行插入和删除。
内存利用率高，不会浪费内存，可以使用内存中细小的不连续的空间，只有在需要的时候才去创建空间。大小不固定，拓展很灵活。

链表的缺点

查找的效率低，因为链表是从第一个节点向后遍历查找。

优化可以通过跳表和增加头尾指针
