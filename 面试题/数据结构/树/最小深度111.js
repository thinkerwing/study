/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 *  *   3
 *     / \
 *   9   20
 *       / \
 *      15  7
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 * 最小深度，考虑使用广度优先遍历
 * 在广度优先遍历过程中，遇到叶子节点，停止遍历，返回节点层级
 */
 var minDepth = function(root) {
    if (!root) { return 0; }
    const q = [[root, 1]]
    while (q.length) {
        const [n, l] = q.shift()
        console.log(n.val, l);
        if (!n.left && !n.right) {
            return l 
        }
        if (n.left) q.push([n.left, l + 1])
        if (n.right) q.push([n.right, l + 1])
    }
};
let res = minDepth([3,9,20,null,null,15,7])
console.log(res);