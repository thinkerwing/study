/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {boolean}
 */
 var findTarget = function (root, k) {
    let arr = []
    const dfs = (root) => {
        if (!root) return false
        arr.push(root.val)
        dfs(root.left)
        dfs(root.right)
    } 
    dfs(root)
    console.log(arr)
    for (let i = 0; i < arr.length; i++) {
        if (arr.indexOf(k-arr[i], i+1) !== -1) return true
    }
    //可选的整数参数。规定在字符串中开始检索的位置。它的合法取值是 0 到 stringObject.length - 1。如省略该参数，则将从字符串的首字符开始检索。 去重
    return false
    
};
