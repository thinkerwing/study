/**
 * @param
  {number[][]} grid
 * @return {number}
 */
  var maxAreaOfIsland = function (grid) {
    let res = 0
    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {
            res = Math.max(res, dfs(grid, i, j))
        }
    }
    return res
};
const dfs = (grid, i, j) => {
    if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length || grid[i][j] !== 1) return 0
    grid[i][j] = 0
    let ans = 1
    ans += dfs(grid, i, j + 1) + dfs(grid, i, j - 1) + dfs(grid, i + 1, j) + dfs(grid, i - 1, j)
    return ans
}