/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 *   3
 *  / \
 * 9   20
 *     / \
 *    15  7
 */
/**
 * @param {TreeNode} root
 * @return {number}
 *  求最大深度，考虑使用深度优先遍历
 *  在深度优先遍历过程中，记录每个节点所在的层级，找出最大的层级即可 
 *  新建一个变量，记录最大深度。
 *  深度优先遍历整颗树，并记录每个节点的层级，同时不断刷新最大深度这个变量
 *  遍历结束返回最大深度这个变量 
 */
 var maxDepth = function(root) {
    let  res = 0;
    const dfs = (n, l) => {
        if (!n) { return; }
        if (!n.left && !n.right) {
            res = Math.max(res, l);
        }
        dfs(n.left, l + 1);
        dfs(n.right, l + 1);
    }
    dfs(root, 1);
    return res;
};
let root =  maxDepth([3,9,20,null,null,15,7])
console.log(root);
