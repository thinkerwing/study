/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 * 输入 1->2->3->4->5->null
 * 输出 5->4->3->2->1->null
 * 双指针一前一后遍历链表
 * 反转双指针
 */
 var reverseList = function(head) {
    let p1 = head;
    let p2 = null;
    while(p1){
        console.log(p1.val, p2 && p2.val);
        const tmp = p1.next // 保存临时变量
        p1.next = p2
        p2 = p1
        p1 = tmp;
    }
    console.log(p1 && p1.val, p2 && p2.val); // p1->null p2->5
    return p2

};