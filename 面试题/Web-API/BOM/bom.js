// navigator
const ua = navigator.userAgent
const isChrome = ua.indexOf('Chrome')
console.log(isChrome);

// screen
console.log(screen.width);
console.log(screen.height);

// location
console.log(location.href); //"https://www.baidu.com/"
console.log(location.protocol);// "https:"
console.log(location.host); // "www.baidu.com"
console.log(location.pathname);
// pathname 属性是一个可读可写的字符串，可设置或返回当前 URL 的路径部分。
console.log(location.search)
console.log(location.hash)

// history
history.back()
history.forward()