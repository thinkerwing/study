// const div1 = document.getElementById('div1')
// console.log('div1', div1);

// const divList = document.getElementsByTagName('div') // 集合
// console.log('divList.length', divList.length);
// console.log('divList[1]', divList[1]);

// const containerList = document.getElementsByClassName('container')
// console.log('containerList.length', containerList.length);
// console.log('containerList[1]', containerList[1]);

// const pList = document.querySelectorAll('p')
// console.log('pList', pList);

const pList = document.querySelectorAll('p')
const p1 = pList[0]

// property 形式
// 修改js变量的属性,不会体现到html结构中
p1.style.width = '100px'
console.log( p1.style.width ); // 100px
p1.className = 'red'
console.log( p1.className );
console.log(p1.nodeName);
console.log(p1.nodeType);

// attribute
// 修改标签的属性，会改变html结构
p1.setAttribute('data-name', 'web')
console.log(p1.getAttribute('data-name'));
p1.setAttribute('style', 'font-size:50px;');