const  div1 = document.getElementById('div1')
const  div2 = document.getElementById('div2')

// 新建节点
const newP = document.createElement('p')
newP.innerHTML = 'this is newP'
// 插入节点
div1.appendChild(newP)

// 移动节点
const p11 = document.getElementById('p1')
div2.appendChild(p11)

// 获取父元素
console.log( p11.parentNode );

// 获取子元素列表
const div1ChildNodes = div1.childNodes
console.log( div1.childNodes )
const div1ChildNodesP = Array.prototype.slice.call(div1.childNodes).filter(child => {
    if (child.nodeType === 1) {
        return true
    }
    return false
})
console.log('div1ChildNodesP', div1ChildNodesP)

div1.removeChild( div1ChildNodesP[0] )

/** 
// 不缓存 DOM 查询结果
for (let i= 0; i < document.getElementsByTagName('p').length; i++ ) {
    // 每次循环，都会计算length，频繁进行 DOM 查询
}
// 缓存 DOM 查询
const pList = document.getElementsByTagName('p')
const length = pList.length
for (let i = 0; i < length; i++) {
    // 缓存length，只进行一次 DOM查询
}
*/

