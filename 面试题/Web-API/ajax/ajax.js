// const xhr = new XMLHttpRequest()
// xhr.open('GET', '/data/test.json', true)
// xhr.onreadystatechange = function () {
//     if (xhr.readyState === 4) {
//         if (xhr.status === 200) {
//             // console.log(
//             //     JSON.parse(xhr.responseText)
//             // )
//             alert(xhr.responseText)
//         } else if (xhr.status === 404) {
//             console.log('404 not found')
//         }
//     }
// }
// xhr.send(null)

function ajax(url) {
    const p = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open('GET', url, true)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) { 
                if (xhr.status === 200) {
                    resolve(
                        JSON.parse(xhr.responseText)
                    )
                } else if (xhr.status === 404 || xhr.status === 500) {
                    reject(new Error('404 not found'))
                }
            }
        }
        xhr.send(null)
    })
    return p
}

const url = '/data/test.json'
ajax(url)
.then(res => console.log(res))
.catch(err => console.error(err))
/* 
1、0：请求未初始化（还没有调用 open（））。

2、1：请求已经建立，但是还没有发送（还没有调用 send（））。

3、2：请求已发送，正在处理中（通常现在可以从响应中获取内容头）。

4、3：请求在处理中；通常响应中已有部分数据可用了，但是服务器还没有完成响应的生成。

5、4：响应已完成；

只有4 和"complete"才可以获取并使用服务器的响应。
如果调用send方法时无须发送请求参数，则使用null作为参数即可。如果直接使用send()方法，则在Internet   Explorer中可以运行，而在Firefox中将不能正常运行。
*/ 
