@[TOC](axios面试题总结)
# axios 是什么
1. Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。前端最流行的 ajax 请求库，
2. react/vue 官方都推荐使用 axios 发 ajax 请求

# axios 特点
1. 基于 promise 的异步 ajax 请求库，支持promise所有的API
2. 浏览器端/node 端都可以使用，浏览器中创建XMLHttpRequests
3. 支持请求／响应拦截器
4. 支持请求取消
5. 可以转换请求数据和响应数据，并对响应回来的内容自动转换成 JSON类型的数据
6. 批量发送多个请求
7. 安全性更高，客户端支持防御 XSRF，就是让你的每个请求都带一个从cookie中拿到的key, 根据浏览器同源策略，假冒的网站是拿不到你cookie中得key的，这样，后台就可以轻松辨别出这个请求是否是用户在假冒网站上的误导输入，从而采取正确的策略。
# axios 常用语法
- axios(config): 通用/最本质的发任意类型请求的方式
- axios(url[, config]): 可以只指定 url 发 get 请求a
- xios.request(config): 等同于axios(config)
- axios.get(url[, config]): 发 get 请求
- axios.delete(url[, config]): 发 delete 请求
- axios.post(url[, data, config]): 发 post 请求
- axios.put(url[, data, config]): 发 put 请求
- axios.defaults.xxx: 请求的默认全局配置
- axios.interceptors.request.use(): 添加请求拦截器
- axios.interceptors.response.use(): 添加响应拦截器
- axios.create([config]): 创建一个新的 axios(它没有下面的功能)
- axios.Cancel(): 用于创建取消请求的错误对象
- axios.CancelToken(): 用于创建取消请求的 token 对象
- axios.isCancel(): 是否是一个取消请求的错误
- axios.all(promises): 用于批量执行多个异步请求axios.spread(): 用来指定接收所有成功数据的回调函数的方法

# axios为什么既能在浏览器环境运行又能在服务器(node)环境运行？
axios在浏览器端使用XMLHttpRequest对象发送ajax请求；在node环境使用http对象发送ajax请求。

```javascript
var defaults.adapter = getDefaultAdapter();
function getDefaultAdapter () {
	var adapter;
    if (typeof XMLHttpRequest !== 'undefined') {
    	// 浏览器环境
        adapter = require('./adapter/xhr');
    } else if (typeof process !== 'undefined') {
    	// node环境
        adapter = require('./adapter/http');
    }
   return adapter;
}
```
上面几行代码，可以看出：XMLHttpRequest 是一个 API，它为客户端提供了在客户端和服务器之间传输数据的功能；process 对象是一个 global （全局变量），提供有关信息，控制当前 Node.js 进程。原来作者是通过判断XMLHttpRequest和process这两个全局变量来判断程序的运行环境的，从而在不同的环境提供不同的http请求模块，实现客户端和服务端程序的兼容。

# axios相比原生ajax的优点
1. ajax的缺点本身是针对MVC的编程,不符合现在前端MVVM的浪潮
2. 基于原生的XHR开发，XHR本身的架构不清晰。
3. JQuery整个项目太大，单纯使用ajax却要引入整个JQuery非常的不合理（采取个性化打包的方案又不能享受CDN服务）
4. 不符合关注分离（Separation of Concerns）的原则配置和调用方式非常混乱，
5. 而且基于事件的异步模型不友好。

**区别**

axios是通过promise实现对ajax技术的一种封装，就像jQuery实现ajax封装一样。 简单来说： ajax技术实现了网页的局部数据刷新，axios实现了对ajax的封装。 axios是ajax ajax不止axios。

从 node.js 创建 http 请求 支持 Promise API 客户端支持防止CSRF（Cross-site request forgery） 提供了一些并发请求的接口（很重要，方便了很多的操作）

# 拦截器
在请求或响应被then或catch处理前拦截它们。 拦截器分为请求拦截器和响应拦截器

请求拦截器（interceptors.requst）是指可以拦截每次或指定HTTP请求，并可修改配置项。
```
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });
```

应用场景:在发起请求之前, 最后对要发送的请求配置对象进行修改

例如: 每个请求都带上的参数，比如token，时间戳等

响应拦截器（interceptors.response）可以在每次HTTP请求后拦截住每次或指定HTTP请求，并可修改返回结果项。

```
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response;
  }, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  });
```

应用场景:在响应回来后, 马上执行响应拦截器函数

例如: 判断错误状态码, 进行相应报错等提示操作;清除过期token

如果你想在稍后移除拦截器，可以这样：

```
var myInterceptor = axios.interceptors.request.use(function () {/*...*/});
axios.interceptors.request.eject(myInterceptor);
```

自定义 axios 实例添加拦截器
```
var instance = axios.create();
instance.interceptors.request.use(function(){/*...*/})
```
# axios原理
createInstance底层根据默认设置 新建一个Axios对象， axios中所有的请求[axios, axios.get, axios.
post等...]内部调用的都是Axios.prototype.request,将Axios.prototype.request的内部this绑定到新建的
Axios对象上,从而形成一个axios实例。新建一个Axios对象时，会有两个拦截器，request拦截器，response拦截器。

# Vue 中 axios 的封装
**通过配置项创建 axios 实例的方式进行配置封装**
```
// http.js
import axios from 'axios'
// 创建 axios 实例
const service = axios.create({
  // 配置项
})
```
**根据环境设置 baseURL**
baseURL 属性是请求地址前缀，将自动加在 url 前面，除非 url 是个绝对地址。正常情况下，在开发环境下和生产模式下有着不同的 baseURL，所以，我们需要根据不同的环境切换不同的 baseURL。
在开发模式下，由于有着 devServer 的存在，需要根据固定的 url 前缀进行请求地址重写，所以，在开发环境下，将 baseURL 设为某个固定的值，比如：/apis。
在生产模式下，根据 Java 模块的请求前缀的不同，可以设置不同的 baseURL。

```
// 根据 process.env.NODE_ENV 区分状态，切换不同的 baseURL
const service = axios.create({
	baseURL: process.env.NODE_ENV === 'production' ? `/java` : '/apis',
})
```

**统一设置请求头**
由于，大部分情况下，请求头都是固定的，只有少部分情况下，会需要一些特殊的请求头，所以，在这里，我采用的方案是，将普适性的请求头作为基础配置。当需要特殊请求头时，将特殊请求头作为参数传入，覆盖基础配置。
```
const service = axios.create({
    ...
	headers: {
        get: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
          // 在开发中，一般还需要单点登录或者其他功能的通用请求头，可以一并配置进来
        },
        post: {
          'Content-Type': 'application/json;charset=utf-8'
          // 在开发中，一般还需要单点登录或者其他功能的通用请求头，可以一并配置进来
        }
  },
})
```

**跨域、超时、响应码处理**
axios 中，提供是否允许跨域的属性——withCredentials，以及配置超时时间的属性——timeout，通过这两个属性，可以轻松处理跨域和超时的问题。
axios 提供了 validateStatus 属性，用于定义对于给定的HTTP 响应状态码是 resolve 或 reject  promise。所以，正常设置的情况下，我们会将状态码为 2 系列或者 304 的请求设为 resolve 状态，其余为 reject 状态。结果就是，我们可以在业务代码里，使用 catch 统一捕获响应错误的请求，从而进行统一处理。
但是，由于我在代码里面使用了 async-await，而众所周知，async-await 捕获 catch 的方式极为麻烦，所以，在此处，我选择将所有响应都设为 resolve 状态，统一在 then 处理。

```
const service = axios.create({
	// 跨域请求时是否需要使用凭证
	withCredentials: true,
    // 请求 30s 超时
	timeout: 30000,
	validateStatus: function () {
		// 使用async-await，处理reject情况较为繁琐，所以全部返回resolve，在业务代码中处理异常
		return true
	},
})
```
**请求、响应处理**
在不使用 axios 的情况下，每次请求或者接受响应，都需要将请求或者响应序列化。
而在 axios 中， transformRequest 允许在向服务器发送请求前，修改请求数据；transformResponse 在传递给 then/catch 前，允许修改响应数据。
通过这两个钩子，可以省去大量重复的序列化代码。

```
const service = axios.create({
    // 在向服务器发送请求前，序列化请求数据
    transformRequest: [function (data) {
        data = JSON.stringify(data)
        return data
    }],
    // 在传递给 then/catch 前，修改响应数据
    transformResponse: [function (data) {
        if (typeof data === 'string' && data.startsWith('{')) {
            data = JSON.parse(data)
        }
        return data
    }]
})

```

**拦截器**
拦截器，分为请求拦截器以及响应拦截器，分别在请求或响应被 then 或 catch 处理前拦截它们。
之前提到过，由于 async-await 中 catch 难以处理的问题，所以将出错的情况也作为 resolve 状态进行处理。但这带来了一个问题，请求或响应出错的情况下，结果没有数据协议中定义的 msg 字段（消息）。所以，我们需要在出错的时候，手动生成一个符合返回格式的返回数据。
由于，在业务中，没有需要在请求拦截器中做额外处理的需求，所以，请求拦截器的 resolve 状态，只需直接返回就可以了。

```
// 请求拦截器
service.interceptors.request.use((config) => {
	return config
}, (error) => {
	// 错误抛到业务代码
    error.data = {}
    error.data.msg = '服务器异常，请联系管理员！'
    return Promise.resolve(error)
})
```

响应拦截器，还是之前的那个问题，除了请求或响应错误，还有一种情况也会导致返回的消息体不符合协议规范，那就是状态码不为 2 系列或 304 时。此时，我们还是需要做一样的处理——手动生成一个符合返回格式的返回数据。但是，有一点不一样，我们还需要根据不同的状态码生成不同的提示信息，以方便处理上线后的问题。

```
// 根据不同的状态码，生成不同的提示信息
const showStatus = (status) => {
    let message = ''
    // 这一坨代码可以使用策略模式进行优化
    switch (status) {
        case 400:
            message = '请求错误(400)'
            break
        case 401:
            message = '未授权，请重新登录(401)'
            break
        case 403:
            message = '拒绝访问(403)'
            break
        case 404:
            message = '请求出错(404)'
            break
        case 408:
            message = '请求超时(408)'
            break
        case 500:
            message = '服务器错误(500)'
            break
        case 501:
            message = '服务未实现(501)'
            break
        case 502:
            message = '网络错误(502)'
            break
        case 503:
            message = '服务不可用(503)'
            break
        case 504:
            message = '网络超时(504)'
            break
        case 505:
            message = 'HTTP版本不受支持(505)'
            break
        default:
            message = `连接出错(${status})!`
    }
    return `${message}，请检查网络或联系管理员！`
}

// 响应拦截器
service.interceptors.response.use((response) => {
    const status = response.status
    let msg = ''
    if (status < 200 || status >= 300) {
        // 处理http错误，抛到业务代码
        msg = showStatus(status)
        if (typeof response.data === 'string') {
            response.data = { msg }
        } else {
            response.data.msg = msg
        }
    }
    return response
}, (error) => {
    // 错误抛到业务代码
    error.data = {}
    error.data.msg = '请求超时或服务器异常，请检查网络或联系管理员！'
    return Promise.resolve(error)
})

```

**完整代码**
```
// http.ts
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'

const showStatus = (status: number) => {
  let message = ''
  switch (status) {
    case 400:
      message = '请求错误(400)'
      break
    case 401:
      message = '未授权，请重新登录(401)'
      break
    case 403:
      message = '拒绝访问(403)'
      break
    case 404:
      message = '请求出错(404)'
      break
    case 408:
      message = '请求超时(408)'
      break
    case 500:
      message = '服务器错误(500)'
      break
    case 501:
      message = '服务未实现(501)'
      break
    case 502:
      message = '网络错误(502)'
      break
    case 503:
      message = '服务不可用(503)'
      break
    case 504:
      message = '网络超时(504)'
      break
    case 505:
      message = 'HTTP版本不受支持(505)'
      break
    default:
      message = `连接出错(${status})!`
  }
  return `${message}，请检查网络或联系管理员！`
}

const service = axios.create({
  // 联调
  baseURL: process.env.NODE_ENV === 'production' ? `/` : '/apis',
  headers: {
    get: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
    },
    post: {
      'Content-Type': 'application/json;charset=utf-8'
    }
  },
  // 是否跨站点访问控制请求
  withCredentials: true,
  timeout: 30000,
  transformRequest: [(data) => {
    data = JSON.stringify(data)
    return data
  }],
  validateStatus () {
    // 使用async-await，处理reject情况较为繁琐，所以全部返回resolve，在业务代码中处理异常
    return true
  },
  transformResponse: [(data) => {
    if (typeof data === 'string' && data.startsWith('{')) {
      data = JSON.parse(data)
    }
    return data
  }]
})

// 请求拦截器
service.interceptors.request.use((config: AxiosRequestConfig) => {
    return config
}, (error) => {
    // 错误抛到业务代码
    error.data = {}
    error.data.msg = '服务器异常，请联系管理员！'
    return Promise.resolve(error)
})

// 响应拦截器
service.interceptors.response.use((response: AxiosResponse) => {
    const status = response.status
    let msg = ''
    if (status < 200 || status >= 300) {
        // 处理http错误，抛到业务代码
        msg = showStatus(status)
        if (typeof response.data === 'string') {
            response.data = {msg}
        } else {
            response.data.msg = msg
        }
    }
    return response
}, (error) => {
    // 错误抛到业务代码
    error.data = {}
    error.data.msg = '请求超时或服务器异常，请检查网络或联系管理员！'
    return Promise.resolve(error)
})

export default service
```