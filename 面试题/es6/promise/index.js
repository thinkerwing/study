const mypromise = require('./mypromise')
let promise = new mypromise((resolve, reject) => {
    // resolve('success')
    // reject('Error')
    // throw new Error('Exception: Error')
    setTimeout(() => {
        resolve('success')
    }, 1000)
})

promise.then((value) => {
    console.log('FullFilled1:' + value)
}, (reason) => {
    console.log('Rejected1:' + reason);
})
promise.then((value) => {
    console.log('FullFilled2:' + value)
}, (reason) => {
    console.log('Rejected2:' + reason);
})