'use strict';
 
var _createClass = function () {
  // target是目标对象，此处是Parent,porps是属性对象数组
  function defineProperties(target, props) {
    // 循环每个元素
    for (var i = 0; i < props.length; i++) {
      // 取出每个属性描述器（这里是一个对象）
      var descriptor = props[i];
      // 可枚举:for in 能循环出来
      descriptor.enumerable = descriptor.enumerable || false;
      // 可配置：可以通过delete删除此属性
      descriptor.configurable = true;
      // 若属性描述器中的value存在，则可修改值
      if ("value" in descriptor) descriptor.writable = true;
      // 真正的向Parent类的原型对象上增加属性
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
 
  // 三个参数分别是 构造函数、原型上的属性、静态属性（类上的属性）
  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();
 
 
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
 
var Parent = function () {
  // 这个方法就相当于构造器constructor
  function Parent(name) {
    _classCallCheck(this, Parent);
 
    this.name = name; // 实例的私有属性
  }
 
  // 属于实例的公有属性，也就是相当于原型上的属性
  _createClass(Parent, [{
    key: 'getName',
    value: function getName() {
      console.log(this.name);
    }
  }]);
 
  return Parent;
}();
 
var p = new Parent('xxx');
p.getName();