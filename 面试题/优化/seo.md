一、语法：
<meta name="name" content="string"> 
二、参数解析：
1)name选项：Keywords(关键字)，description(网站内容描述)，author(作者)，robots(机器人向导)等等 

2)http-equiv项：可用于代替name项，常用的选项有Expires(期限)，Pragma(cache模式)，Refresh(刷新)，Set-Cookie(cookie设定)，Window-target(显示窗口的设定)，content-Type(显示字符集的设定)等。 
3）content项：根据name项或http-equiv项的定义来决定此项填写什么样的字符串。

1.添加对手机设备的支持
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user- 
scalable=no">（禁止缩放）
2.兼容IE
<meta http-equiv="X-UA-Compatible" content="IE=edge">
3.规定utf-8编码
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
4.让搜索引擎搜索到关键字
<meta name="Keywords" content="网页关键字">
<meta name="description" content="网页描述文字" />
5.设置双核浏览器的浏览模式
<meta name="renderer" content="webkit">
<meta name=“renderer” content=“webkit|ie-comp|ie-stand”>（分别为极速模式，兼容模式，以 
及IE模式）

6.产生特殊效果

< meta http-equiv="Page-Enter" content= "revealTrans(Duration=5.0,Transition=n)" >(n的取值范围为0-23)

7、标注作者： 
<meta name="author" content="二度空间"> 
8、控制页面缓冲，如不要页面缓冲的代码这样写： 
<meta http-equiv="Cache-Control" CONTENT="no-cache">

9.让网页每隔一段时间刷新一次，若要10秒刷新一次，代码这样写： 
<meta http-equiv="refresh" content="10"> 

10.让一个页面过上一定的时间，自动转到另一个页面或者站点去，如： 
< Meta HTTP-EQUIV="refresh" content="6; url=http://hi.baidu.com/tesalo/" >

content中的6表示时间，单位为秒，url=后面是你要转向的网址，若是与你当前网页在同一目录下，可以直接写上文件名，如： 
< Meta HTTP-EQUIV="refresh" content="6; url=page1.htm" >

可以参考的网址：http://blog.csdn.net/jamesmgw/article/details/7320090；http://blog.sina.com.cn/s/blog_6f2ea7950100xjtb.html；

原文地址：https://www.cnblogs.com/no-broken-boy/p/5124458.html