1. 以下说法对的是
let a = Object.create(null); let b = Object.create({x: 12})
A、a.__proto__为undefined，b的x属性为12
B、a.__proto__为undefined，b.__proto__的x属性值为12
C、a.__proto__为{}, b:x 为12

a.__proto__为undefined，b.x为12 b.__proto__ {x: 12}