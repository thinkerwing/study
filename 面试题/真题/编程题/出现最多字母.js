/*
实现一个函数findMaxDuplicateChar，统计一个字符串中出现最多的字母。
例如 var str = "afjghdfraaaaasdenas"，调用该函数后findMaxDuplicateChar(str)，会返回'a'
*/
function test(str) {
    let map = new Map(), res
    for (let i = 0; i < str.length; i++) {
        let c = str[i]
        map.set(c, map.has(c) ? map.get(c) + 1 : 1)
    }
    let max = Math.max(...map.values())
    map.forEach((value, key) => {
        if (max == map.get(key)) {
            res = key
        }
    })
    return res
}
let res = test(str = "afjghdfraaaaasdenasccccccccc")
console.log(res);