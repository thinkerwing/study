function urlToObj(str) {
    var obj = {};
    var arr1 = str.split("?");
    var arr2 = arr1[1].split("&");
    for (var i = 0; i < arr2.length; i++) {
        var res = arr2[i].split("=");
        if (res[1] == undefined) { res[1] = ''}
        obj[res[0]] = res[1];
    }
    return obj;
}
var url = "http://www.baidu.com?a=1&b=2&c=3&d";
let res = urlToObj(url);
console.log(res);