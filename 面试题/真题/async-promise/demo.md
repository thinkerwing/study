1. 有关async、promise、generator正确的是
A、async/await配合使用可以取代promise
B、promise是generator进一步完善的标准，generator可以逐步淘汰了
C、async/await配合使用是阻塞的，await fn(),fn函数没执行完，后面的代码不会执行

async / await不会阻止整个主线程. node.js仍然将所有Javascript作为单线程运行,即使某些代码在async / await上等待,其他事件仍然可以运行其事件处理程序(因此node.js不会被阻止).事件队列仍在为其他事件提供服务.实际上,它将是一个事件,它解决了一个允许等待停止等待并运行以下代码的承诺.


