class classA {
    constructor() {
        this.data = {
            name: 'tom',
            age: 30
        }
    }
    valueOf() {
        return `'valueOf' + ${this.name} + 'is' + ${this.data}`
    }
}
const objA = {
        data: {
            name: 'tom',
            age: 30
    },
    toString() {
        return `'toString' + ${this.name} + 'is' + ${this.data}`
    }
}
let a = new classA()
console.log(a)
console.log(objA);