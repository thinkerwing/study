/*
    当您有多个不依赖于彼此成功完成的异步任务时，通常会使用它，或者您总是想知道每个承诺的结果。
    相比之下，Promise.all()如果任务相互依赖/如果您想在其中任何一个拒绝时立即拒绝，则返回的 Promise可能更合适。
    expected output:
    "fulfilled"
    "rejected"
*/ 

const promise1 = Promise.resolve(3);
const promise2 = new Promise((resolve, reject) => setTimeout(reject, 100, 'foo'));
const promises = [promise1, promise2];

Promise.allSettled(promises).
  then((results) => results.forEach((result) => console.log(result.status)));