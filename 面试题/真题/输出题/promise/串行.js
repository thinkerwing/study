const p1 = (preResult) => {
    // preResult为前一个Promise返回值，由于p1之前没有Promise，此处为undefined
    return new Promise((resolve, reject) => {
        console.log('pre return value: ', preResult);
        resolve(11)
    });
}
const p2 = (preResult) => {
    // preResult为前一个Promise返回值，此处即p1的返回值11
    return new Promise((resolve, reject) => {
        console.log('pre return value: ', preResult);
        resolve(22)
    });
}
const p3 = (preResult) => {
    // preResult为前一个Promise返回值，此处即p2的返回值22
    return new Promise((resolve, reject) => {
        console.log('pre return value: ', preResult);
        resolve(33)
    });
}

p1().then(p2).then(p3).then((result) => {
    // result为链式调用中最后一个Promise的返回值，此处为p3返回值33
    console.log('last return value: ', result);
})