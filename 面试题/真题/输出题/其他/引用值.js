let a = [1, 2, 3];
let b = { aa: 1, bb: 2 };

function ab(a, b) {
  a.push(4);
  a = [];
  b.aa = 3;
  b = {};
}
ab(a, b);
console.log("a:", a, "b:", b);
//a: [ 1, 2, 3, 4 ] b: { aa: 3, bb: 2 }