function someFunction() {
    let a = 0
    return function() {
        return a++
    }
}
let f1= someFunction()
let f2= someFunction()

console.log(f1())
console.log(f2())

let f = someFunction()
console.log(f())
console.log(f())

// 0001

function fn() {
    let a = b = 100
    window.x = y = 101
}
fn()
console.log(b,x,y);
// 100 101 101
// console.log(a); //Uncaught ReferenceError: a is not defined
// let a = b = 100 可以分为let a = 100 b = 100
// window上面挂在的变量只有在非函数作用域的var声明或者直接b=1

