function deepClone(obj) {
	let newObj = null
	if (typeof obj === 'object' && obj !== 'null') {
		newObj = obj instanceof Array ? [] : {}
		for (let i in obj) {
			newObj[i] = deepClone(obj[i])
		}
	} else {
		newobj = obj
	}
	return newObj
}

let obj1 = {
	name: 'think',
	age: 20,
	info: {
		school: 'fit',
	},
	arr: [1, 2, 3, { test: 'HHH' }, [5, 1]],
	pNext: null,
}
let obj2 = deepClone(obj1)
obj2.info.school = 'zw'
console.log(obj2)
console.log(obj1)
