function person(name) {
    this.name = name; 
}
let a = new person('a.name')
console.log(a.name);  //a.name
function _new(obj, ...args) {
    // 基于obj的原型创建一个新的对象
    let res = Object.create(obj.prototype);
    // 添加属性到新创建的res上, 并获取obj函数执行的结果.
    let result = obj.apply(res, args);
    // 如果执行结果有返回值并且是一个对象, 返回执行的结果, 否则, 返回新创建的对象
    return  typeof result === 'object' ? result : res;
}
let b = _new(person,'b.name');
console.log(b.name);  //b.name