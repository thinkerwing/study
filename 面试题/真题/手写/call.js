//传递参数从一个数组变成逐个传参了,不用...扩展运算符的也可以用arguments代替
Function.prototype.call2 = function (context, ...args) {
    //这里默认不传就是给window,也可以用es6给参数设置默认参数
    context = context || window
    args = args ? args : []
    //给context新增一个独一无二的属性以免覆盖原有属性
    const key = Symbol()
    context[key] = this
    //通过隐式绑定的方式调用函数
    const result = context[key](...args)
    //删除添加的属性
    delete context[key]
    //返回函数调用的返回值
    return result
}

const obj1 = {
    name: 'joy',
    getName() {
        console.log(this.name); 
    }
};

const obj2 = {
    name: 'call'
};

obj1.getName.call(obj2); //obj2 call
obj1.getName.call2(obj2); //obj2 call
obj1.getName.apply(obj2); //obj2 call
const fn = obj1.getName.bind(obj2);
fn();//obj2 call