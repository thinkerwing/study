let arr = [5, 4, 2, 2, 3, 1]
function quickSort(arr) {
    if (arr.length < 2) return arr
    let pivot = arr[arr.length-1]
    let left = arr.filter((v, i) =>  v <= pivot && i !== arr.length - 1)
    let right = arr.filter((v) =>  v > pivot)
    return [...quickSort(left), pivot, ...quickSort(right)]
}
console.log(quickSort(arr));

//箭头函数后面没有加大括号并且只有一行代码返回，是会默认return的；
//而箭头函数后面加了大括号，则不会默认return