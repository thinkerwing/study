跨域遇到的笔试题

1. 解决跨域的方案，以下说法对的是
A、可以利用flash的http请求，来处理跨域问题
B、通过iframe设置document.domain可以实现跨域
C、一般情况下，m.toutiao.com可以ajax请求www.toutiao.com域名下的接口并获得响应
D、通过jsonp方式可以发出post请求其他域名下的接口

正确答案：B

解析：
B、站内AJAX跨域可以通过document.domain和iframe实现，document.domain；这种方式用在主域名相同子域名不同的跨域访问中
C、同源策略
D、JSONP的缺点则是：它只支持GET请求而不支持POST等其它类型的HTTP请求；它只支持跨域
HTTP请求这种情况，不能解决不同域的两个页面之间如何进行JavaScript调用的问题。

2. 解决跨域的方案，以下说法对的是
A、如果接口为post请求，后端人员将接口的method改为get可以解决
B、jsonp的内部是ajax实现的
C、建议在线上使用webpack devServer proxy解决
D、建议线上接口修改Nginx增加header头解决

选 C
B、jsonp请求；jsonp的原理是利用<script>标签的跨域特性，可以不受限制地从其他域中加载资源，类似的标签还有<img>.
JSONP 核心原理script 标签不受同源策略影响。动态插入到 DOM 中的 script 脚本可以立即得到执行。和ajax没有关系。
D、nginx做反向代理用的，响应头的设置是后端

3. 有关xss预防说法正确的是
A、过滤一切用户输入和并在服务端录入和客户端输出时候做同样的处理
B、只需要过滤用户输入即可
C、通过阻止客户端输入script标签可以阻止攻击

4.下面不属于TCP协议拥塞控制部分的是
A、带外数据
B、快速恢复
C、快速重传
D、慢启动

选 A
TCP的拥塞控制由4个核心算法组成：“慢启动”（Slow Start）、“拥塞避免”（Congestion voidance）、“快速重传 ”（Fast Retransmit）、“快速恢复”（Fast Recovery）

多选题：
5. http2的说法错误的是
支持多路复用，交错的多个并行的请求或者响应，而不需要阻塞
使用http2，网站就100%安全
默认强制https协议
http2虽然有很多好处，但是拖慢网站的加载速度

HTTP/2 相比于 HTTP/1.1，可以说是大幅度提高了网页的性能，只需要升级到该协议就可以减少很多之前需要做的性能优化工作