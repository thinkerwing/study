// 1 使用commonjs的模块化规范
const {add, mul} = require('./js/mathUtils')
console.log(add(20, 30));
console.log(mul(10,20));
// 2 使用ES6的模块化的规范
import { name, age } from './js/info'
console.log(name);
console.log(age);
// 3 依赖css文件
require('./css/normal.css')
// 4 依赖less文件
require('./css/special.less')
document.writeln('<h2>hello，world</h2>') // 自动换行
// 5 使用vue进行开发
import Vue from 'vue'
// import App from './vue/app'
import App from './vue/App.vue'

new Vue({
  el: '#app',
  template: '<App/>',
  components: {
    App
  }
})
