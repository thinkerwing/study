import {
    init,
    classModule,
    propsModule,
    styleModule,
    eventListenersModule,
    h,
} from "snabbdom";

// 创建patch函数
const patch = init([
    classModule,
    propsModule,
    styleModule,
    eventListenersModule,
]);

// 创建虚拟节点
var vnode1 = h(
    "ul", {}, [
        h('li', { key: 'A' }, 'A'),
        h('li', { key: 'B' }, 'B'),
        h('li', { key: 'C' }, 'C'),
        h('li', { key: 'D' }, 'D')
    ]
);

// 让虚拟节点上树
let container = document.getElementById("container");
patch(container, vnode1);

var vnode2 = h(
    "ul", {}, [
        h('li', { key: 'E' }, 'E'),
        h('li', { key: 'A' }, 'A'),
        h('li', { key: 'B' }, 'B'),
        h('li', { key: 'C' }, 'C'),
        h('li', { key: 'D' }, 'D')
    ]
);

//  点击按钮时, 将vnode1变为vnode2
btn.onclick = function() {
    patch(vnode1, vnode2)
}