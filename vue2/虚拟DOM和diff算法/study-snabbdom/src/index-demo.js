import {
    init,
    classModule,
    propsModule,
    styleModule,
    eventListenersModule,
    h,
} from "snabbdom";

// 创建patch函数
const patch = init([
    classModule,
    propsModule,
    styleModule,
    eventListenersModule,
]);

// 创建虚拟节点
var myVnode1 = h(
    "a", {
        props: {
            href: "https://www.baidu.com",
            target: "_blank"
        }
    },
    "baidu"
);

// 一个容器只能让一个虚拟节点上树
const myVnode2 = h('div', {class: {'box': true}},'我是一个盒子')

const myVnode3 = h('ul', [
    h('li', '苹果'),
    h('li', '香蕉'),
    h('li',  [
        h('div', '西瓜籽'),
    ]),
    h('li', '番茄'),
  ])

// 让虚拟节点上树
let container = document.getElementById("container");
patch(container, myVnode3);