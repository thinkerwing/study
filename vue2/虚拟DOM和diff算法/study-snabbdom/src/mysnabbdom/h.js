import vnode from "./vnode";
/**
 * 产生虚拟DOM树，返回的一个对象
 * 低配版本的h函数，这个函数必须接受三个参数，缺一不可
 * 调用只有三种形态 文字、数组、h函数
 * ① h('div', {}, '文字')
 * ② h('div', {}, [])
 * ③ h('div', {}, h())
 */
export default function (sel, data, c) {
  // 检查参数个数
  if (arguments.length !== 3) {
    throw new Error("h函数必须传入3个参数");
  }
  // 检查第参数c的类型
  if (typeof c === "string" || typeof c === "number") {
    // 说明现在调用h函数是形态1
    return vnode(sel, data, undefined, c, undefined);
  } else if (Array.isArray(c)) {
    // 说明现在调用h函数是形态2 数组
    let children = [];
    // 遍历 c 数组，收集children
    for (let item of c) {
      // 检查c[i]必须是一个对象，如果不满足 
      if (!(typeof item === "object" && item.hasOwnProperty("sel"))) {
        throw new Error("传入的数组参数中有项不是h函数");
      }
      // 不用执行item, 因为测试语句中已经有了执行，此时只要收集数组中的每一个对象
      children.push(item);
    }
    //循环结束了，就说明children收集完毕了，此时可以返回虚拟节点了，它有children属性
    return vnode(sel, data, children, undefined, undefined);
  } else if (typeof c === "object" && c.hasOwnProperty("sel")) {
    // 说明现在调用h函数是形态3 即，传入的c是唯一的children，不用执行c
    let children = [c];
    return vnode(sel, data, children, undefined, undefined);
  } else {
    throw new Error("传入的第三个参数类型不对");
  }
}
