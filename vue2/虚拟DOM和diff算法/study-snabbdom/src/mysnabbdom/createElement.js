  // 真正创建节点。将vnode虚拟节点创建为DOM，是孤儿节点，不进行插入
 export default function createElement(vnode) {
    console.log('目的是把虚拟节点', vnode, '真正变为DOM');
    // 根据虚拟节点sel选择器属性 创建一个DOM节点，这个节点现在是孤儿节点
    let domNode = document.createElement(vnode.sel);
    // 判断是有子节点还是有文本
    if ( vnode.text !== "" && (vnode.children === undefined || vnode.children.length === 0)
    ) {
      // 说明内部是文本
      domNode.innerText = vnode.text;
      // 这里不上树，因为设置内部文字就相当于上树
    } else if (Array.isArray(vnode.children) && vnode.children.length > 0) {
      // 说明内部是子节点，需要递归创建节点 
      for (let i = 0; i < vnode.children.length; i++) {
        // 得到当前这个children
        let ch = vnode.children[i]
        // 创建出它的DOM，一旦调用createElement意味着：创建出DOM了，并且它的elm属性指向了创建出的DOM，
        // 但是还没上树，是一个孤儿节点
        console.log(ch);
        let chDOM = createElement(ch)
        // n+1层就可以被n层创建回来
        // 上树
        domNode.appendChild(chDOM)
      }
    }
    // 补充虚拟节点的elm属性
    vnode.elm = domNode;
    // 返回domNode 和 vnode.elm 是一样的 引用类型值内存中同一个对象，elm的属性是一个纯DOM对象
    return domNode;
  }