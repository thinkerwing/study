import vnode from './vnode';
import createElement from './createElement';
import patchVnode from './patchVnode.js';

export default function (oldVnode, newVnode) {
  // 判断传入的第一个参数是 DOM节点 还是 虚拟节点
  if (oldVnode.sel == '' || oldVnode.sel === undefined) {
    // 传入的第一个参数是DOM节点，此时要包装成虚拟节点
    oldVnode = vnode(
      oldVnode.tagName.toLowerCase(), // sel
      {}, // data
      [], // children
      undefined, // text
      oldVnode // elm
    );
  }
  // 判断 oldVnode 和 newVnode 是不是同一个节点
  if (oldVnode.key === newVnode.key && oldVnode.sel === newVnode.sel) {
    console.log('是同一个节点');
    patchVnode(oldVnode, newVnode)
  } else {
    console.log("不是同一个节点，暴力插入新节点，删除旧节点");
    let newVnodeElm = createElement(newVnode);
    let oldVnodeElm = oldVnode.elm;
    // 以oldVnodeElm为标杆
    if (oldVnodeElm.parentNode && newVnodeElm) {
      // 判断newVnodeElm是存在的，插入到老节点之前
      oldVnodeElm.parentNode.insertBefore(newVnodeElm, oldVnodeElm);
    }
    // 删除老节点
    oldVnodeElm.parentNode.removeChild(oldVnodeElm);
  }
}
