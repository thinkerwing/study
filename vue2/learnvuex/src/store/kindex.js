import Vue from 'vue'
import kvuex from '../kvuex'

Vue.use(kvuex)

export default new kvuex.Store({
  state: {
    count: 0
  },
  getters: {
    score(state) {
      return 'score:' + state.count
    }
  },
  mutations: {
    add (state, num = 1) {
      state.count += num
    }
  },
  actions: {
    // 复杂业务逻辑，类似controller
    // 比如ajax请求
    addasync({commit}) {
      return new Promise(resolve => {
        setTimeout(() => {
          commit('add')
          resolve({ok: 1})
        }, 1000)
      })
    }
  }
})
