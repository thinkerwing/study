import Vue from 'vue'
import App from './App'
// import store from './store'
import store from './store/kindex' // 模拟实现的vuex
console.log(store);

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
