var isCovered = function (ranges, left, right) {
    let set = new Set()
    for (let i = 0; i < ranges.length; i++) {
        let L = ranges[i][0]
        let R = ranges[i][1]
        while (L <= R) {
            set.add(L)
            L++
        }
    }
    for (let i = left; i <= right; i++) {
        if (!set.has(i)) {
            return false
        }
    }
    return true
};
let res = isCovered(ranges = [
    [1, 1]
], left = 1, right = 50)
console.log(res);