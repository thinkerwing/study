/**
 * @param {number[]} g
 * @param {number[]} s
 * @return {number}
 * 输入: g = [1,2,3], s = [1,1]
输出: 1
解释: 
你有三个孩子和两块小饼干，3个孩子的胃口值分别是：1,2,3。
虽然你有两块小饼干，由于他们的尺寸都是1，你只能让胃口值是1的孩子满足。
所以你应该输出1。
输入: g = [1,2], s = [1,2,3]
输出: 2

解题思路：既能满足孩子，还消耗最少
先将较小的饼干分给胃口最小的孩子
解题步骤：对饼干数组和胃口数组升序排序
遍历饼干数组，找到能满足第一个孩子的饼干
然后继续遍历饼干数组，找到满足第二、三、..n孩子的饼干
 */
 var findContentChildren = function(g, s) {
    const sortFunc = function(a, b) {
        return a - b
    }
    g.sort(sortFunc)
    s.sort(sortFunc)
    let i = 0
    s.forEach( n => {
        if (n >= g[i]) {
            i += 1
        }
    })
    return i
};
let res = findContentChildren(g = [1,2,3], s = [1,1])
let res2 = findContentChildren(g = [1,2], s = [1,2,3])
console.log(res);
console.log(res2);