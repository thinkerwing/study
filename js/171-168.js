/**
 * @param {number} columnNumber
 * @return {string}
 */
var convertToTitle = function (n) {
    let res = '';
    while( n ) {
        const num = n % 26 ? n % 26 : 26; // 取模； 0则取26
        res = String.fromCharCode( num + 65 - 1 ) + res; // 获取A-Z字符串
        n = parseInt( (n-num) / 26 ); // 取余 parseInt() 函数可解析一个字符串，并返回一个整数。
    }
    return res;
    
};
let res = convertToTitle(28)
console.log('res->', res);