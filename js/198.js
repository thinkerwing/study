/**
 * 你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。

给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。

示例 1：

输入：[1,2,3,1]
输出：4
解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
     偷窃到的最高金额 = 1 + 3 = 4 。

*/
/**
 * @param {number[]} nums
 * @return {number}
 */
 var rob = function(nums) {
    if(nums.length === 0) {return 0;}
    const dp = [0, nums[0]]
    console.log(dp[0]);
    // 设置临界值 下标为0的情况 打劫0 
    for(let i = 2; i <= nums.length; i+=1 ) {
        dp[i] = Math.max(dp[i-2] + nums[i-1], dp[i-1])
        // i room 1 i=2  so i-1
        // dp[i-1] 不打劫当前房屋
        // when i = 2 ,dp[0] + nums [1] | dp[1]
        //i = 3 , dp [1] + nums[2] | dp[2]
     }
     // dp数组永远比nums数组大1
     console.log(dp);
     return dp[nums.length]

};
let res = rob([2, 7 ,9, 3, 1])
console.log(res);
/**
 * f(k) = 从前k个房屋中能偷窃到的最大数额
 * Ak = 第k个房屋的钱数
 * 定义子问题f(k) = max(f(k-2)+Ak,f(k-1))
 * 使用动态规划
 * 反复执行：从2循环到n，执行上述公式
 */