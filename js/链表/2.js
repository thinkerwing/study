/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 * 
输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[7,0,8]
解释：342 + 465 = 807.
 */
var addTwoNumbers = function (l1, l2) {
    let dummy = new ListNode()
    let curr = dummy
    let carry = 0
    while (l1 !== null || l2 !== null) {
        let sum = 0
        if (l1 !== null) {
            sum += l1.val
            l1 = l1.next
        }
        if (l2 !== null) {
            sum += l2.val
            l2 = l2.next
        }
        sum += carry
        curr.next = new ListNode(sum % 10)
        carry = Math.floor(sum / 10)
        curr = curr.next
    }
    if (carry > 0) {
        curr.next = new ListNode(carry)
    }
    return dummy.next
};
/*
curr 是用来遍历列表用的，一开始指向头部，carry是处理进位的变量，一开始是0
while循环，只要l1或l2有一个不是空的，就统计两个的和，还需要进位即l1=l1.next

*/