/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 * 
    输入：head = [1,2,3,4,5], n = 2
    输出：[1,2,3,5]
 */
var removeNthFromEnd = function (head, n) {
    let dummy = new ListNode()
    dummy.next = head

    let n1 = dummy
    let n2 = dummy
    for (let i = 0; i < n; i++) {
        n2 = n2.next
    }

    while (n2.next !== null) {
        n1 = n1.next
        n2 = n2.next
    }
    n1.next = n1.next.next
    return dummy.next
}