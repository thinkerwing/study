/**
 * @param {string} s
 * @return {string}
 */
// 简写frequencysort()
var frequencySort = function(s, map = new Map()) {
    for(let i = 0; i < s.length; i++) {
        let  c = s[i]
        map.set(c, map.has(c) ? map.get(c) + 1 : 1)
    }
    return Array.from(map).sort((a,b) => b[1] - a[1]).map( str => str[0].repeat(str[1])).join('')
}

// 思路
// a[0]是key a[1]是value key.repeat[value] value 是次数 所以复制出现的次数
let res = frequencySort("tree")
console.log('res->', res);


 var frequencySort2 = function(s) {
   const map = new Map()
   for (let i = 0; i < s.length; i++) {
       let c = s[i]
       map.set(c, map.has(c) ? map.get(c) + 1 : 1)
   }
   let a = Array.from(map)
   console.log(a);
   a.sort((a, b) => b[1] - a[1])
   console.log(a,'a');
   let d = a.map( str => str[0].repeat(str[1])).join('')
   console.log(d);
};
let res2 = frequencySort2("tree")
console.log(res2);
// map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值。
// let array = [1, 2, 3, 4, 5];
// let newArray = array.map( i => {
//     return i
// })
// console.log(newArray) 

