/**假设你正在爬楼梯。需要 n 阶你才能到达楼顶。

每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

注意：给定 n 是一个正整数。

示例 1：

输入： 2
输出： 2
解释： 有两种方法可以爬到楼顶。
1.  1 阶 + 1 阶
2.  2 阶 
示例 2：

输入： 3
输出： 3
解释： 有三种方法可以爬到楼顶。
1.  1 阶 + 1 阶 + 1 阶
2.  1 阶 + 2 阶
3.  2 阶 + 1 阶*/

/**
 * @param {number} n
 * @return {number}
 */
 var climbStairs = function(n) {
    if ( n < 2 ) { return 1;}
    // const dp = [1, 1] // 用两个变量 降低空间复杂度
    let dp0 = 1
    let dp1 = 1
    // n不能为0 下标代表n 数字代表多少种方法
    // 0 阶 1种方法 1阶 1种方法
    for (let i = 2; i <=n; i++) {
        // dp[i] = dp[i-1] + dp[i-2]
        const temp = dp0
        dp0 = dp1
        dp1 = dp1 + temp
    }
    console.log(dp1);
    // return dp[n]
    return dp1
};
let res = climbStairs(4)
console.log('res->', res);
/** 思路
 * 爬到第n阶台阶可以在第n-1阶爬1个台阶，
 * 或者在第n-2阶爬2个台阶
 * F(n) = F(n-1)+F(n-2)
 * 
 */