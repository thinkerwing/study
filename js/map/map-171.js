/**
 * @param {string} columnTitle
 * @return {number}
 */
 var titleToNumber = function(columnTitle, map = new Map()) {
    for(let i = 0;i < 26; i++){
        map.set('A'.charCodeAt()+i,i+1);
    }
    console.log(columnTitle.split(""));
    let arr = Array.from(map)
    // console.log(arr);
    let len = columnTitle.length;
    let result = 0
    if (len < 2) {
        return map.get(columnTitle.charCodeAt());
    } else {
        for (let i = 0; i < len; i++) {
            // 根据map取得字母对应的数字
            result += map.get(columnTitle.split("")[i].charCodeAt()) * Math.pow(26, len - i - 1);
        }
        return result
    }
};
let res = titleToNumber("ZY")
console.log('res->', res);