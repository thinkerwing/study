/**
 * @param {number[]} nums
 * @return {number}
 */
 var singleNumber = function(nums) {
    let map = new Map()
    for (let i = 0; i < nums.length; i++) {
        let c = nums[i]
        map.set(c, map.has(c) ? map.get(c) + 1 : 1)
    }
    let a
    map.forEach((value, key) => {
        if (value == 1) a = key
    })
    return a
};
   let res = singleNumber(nums = [0,1,0,1,0,1,100])
   console.log(res);