
/**数组中占比超过一半的元素称之为主要元素。给你一个 整数 数组，找出其中的主要元素。若没有，返回 -1 。
 * 请设计时间复杂度为 O(N) 、空间复杂度为 O(1) 的解决方案。

 * @param {number[]} nums
 * @return {number}
 */
 var majorityElement = function(nums, map = new Map()) {
    
    for(let i = 0; i < nums.length; i++) {
            let  c = nums[i]
            map.set(c, map.has(c) ? map.get(c) + 1 : 1)
        }
        let arr = []
       let b =  Array.from(map)
       for(let i = 0; i < b.length; i++) {
           let len = nums.length/2
           if(Math.abs(b[i][1]) > len) {
            arr.push(b[i][0])
           } else {
            arr.push(-1) 
           }
        }
          let res = arr.filter( item => item>-1 || item <-1)
           return res.length > 0 ? res[0]:  -1

    };

    let res = majorityElement(nums=[1,2,5,9,5,9,5,5,5])
    console.log('res->', res);
    let res2 = majorityElement(nums=[3,2])
    console.log('res2->', res2);
    let res3 = majorityElement(nums=[6,5,5])
    console.log('res3->', res3);
    let res4 = majorityElement(nums=[-1111])
    console.log('res4->', res4);
    let res5 = majorityElement(nums=[1])
    console.log('res5->', res5);