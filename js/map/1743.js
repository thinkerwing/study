var restoreArray = function (adjacentPairs) {
    let map = new Map()
    for (const adjacentPair of adjacentPairs) {
        console.log(adjacentPair);
        map.get(adjacentPair[0]) ? map.get(adjacentPair[0]).push(adjacentPair[1]) : map.set(adjacentPair[0], [adjacentPair[1]]);
        map.get(adjacentPair[1]) ? map.get(adjacentPair[1]).push(adjacentPair[0]) : map.set(adjacentPair[1], [adjacentPair[0]]);
    }
    const n = adjacentPairs.length + 1;
    const ret = new Array(n).fill(0);
    for (const [e, adj] of map.entries()) {
        if (adj.length === 1) {
            ret[0] = e;
            break;
        }
    }
    console.log(ret);
    ret[1] = map.get(ret[0])[0];
    console.log('ret', ret);
    for (let i = 2; i < n; i++) {
        const adj = map.get(ret[i - 1])
        // console.log(adj);
        ret[i] = ret[i - 2] == adj[0] ? adj[1] : adj[0]
    }
    return ret

};
let res = restoreArray([
    [2, 1],
    [3, 4],
    [3, 2]
])
console.log(res);