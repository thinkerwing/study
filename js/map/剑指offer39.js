/**
 * @param {number[]} nums
 * @return {number}
 * 
    输入: [1, 2, 3, 2, 2, 2, 5, 4, 2]
    输出: 2
 */
var majorityElement = function (nums, map = new Map()) {
	for (let i = 0; i < nums.length; i++) {
		let c = nums[i]
		map.set(c, map.has(c) ? map.get(c) + 1 : 1)
	}
	let res
	map.forEach((value, key) => {
		if (value > nums.length / 2) {
			res = key
		}
	})
	return res
}
var majorityElement1 = function (nums, map = new Map()) {
	let a
	for (let i = 0; i < nums.length; i++) {
		let c = nums[i]
		map.set(c, map.has(c) ? map.get(c) + 1 : 1)
	}
	values = Array.from(map.values())
	keys = Array.from(map.keys())
	for (let i = 0; i < values.length; i++) {
		if (values[i] > Math.floor(nums.length / 2)) {
			b = values[i]
		}
	}
	for (let i = 0; i < keys.length; i++) {
		if (map.get(keys[i]) === b) {
			return keys[i]
		}
	}
}
let res = majorityElement([1, 2, 3, 2, 2, 2, 5, 4, 2])
console.log(res)
let res2 = majorityElement1([2, 2, 1, 1, 1, 2, 2])
console.log(res2)
