/**
 * @param {string} s
 * @return {number}
 * 
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
输入: "III"
输出: 3
输入: "IV"
输出: 4
 */
 var romanToInt = function(s, map = new Map()) {
    map = map.set('I', 1)
    map = map.set('V', 5)
    map = map.set('X', 10)
    map = map.set('L', 50)
    map = map.set('C', 100)
    map = map.set('D', 500)
    map = map.set('M', 1000)
    console.log(map);
    let sum = 0
    for(i = 0; i < s.length; i++){
        const a = map.get(s[i])
        if (i < s.length  && a < map.get(s[i+1])){
            sum -= a 
        } else {
            sum += a   
        }
    }
    return sum
};
let res = romanToInt("CDD")
console.log(res,'res');