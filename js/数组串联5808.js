/**
 * 给你一个长度为 n 的整数数组 nums 。请你构建一个长度为 2n 的答案数组 ans ，数组下标 从 0 开始计数 ，
 * 对于所有 0 <= i < n 的 i ，满足下述所有要求：
ans[i] == nums[i]
ans[i + n] == nums[i]
具体而言，ans 由两个 nums 数组 串联 形成。

返回数组 ans 。
 * @param {number[]} nums
 * @return {number[]}
 */
 var getConcatenation = function(nums) {
    let n = nums.length
    for (let i = 0; i < n; i++) {
        nums[i+n] = nums[i]
    }
    return nums
};
var getConcatenation2 = function(nums) {
    return nums.concat(nums)
};
let res = getConcatenation([1,2,1])
console.log('res',res);
let res2 = getConcatenation2([1,2,1])
console.log('res2',res2);