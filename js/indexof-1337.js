const kWeakestRows = (mat, k) => {
    let arr = [];
    for (let i = 0; i < mat.length; i++) {
        // 查找每行中1的个数
        const num = mat[i].indexOf(0) === -1 ? mat[i].length : mat[i].indexOf(0);
        arr.push([i, num]);
    }
    console.log(arr);
    arr.sort((a, b) => a[1] - b[1]);
    console.log(arr);
    arr = arr.map(item => item[0]);
    arr.splice(k);
    return arr;
};
let res = kWeakestRows(
	mat = [
		[1, 1, 0, 0, 0],
		[1, 1, 1, 1, 0],
		[1, 0, 0, 0, 0],
		[1, 1, 0, 0, 0],
		[1, 1, 1, 1, 1],
	],
	k = 3
)
console.log(res)