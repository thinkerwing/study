/**
 * @param {number[]} citations
 * @return {number}
 */
 var hIndex = function(citations) {
    let len = citations.length;
    let left = 0;
    let right = len - 1
    while(left <= right) {
        const mid = left + Math.floor((right - left) /2)
        if (citations[mid] >= len - mid) {
            right = mid -1
        } else {
            left = mid + 1
        }
    }
    return len - left

    // console.log(citations);
};
let res = hIndex([0,1,3,5,6])
console.log(res);
