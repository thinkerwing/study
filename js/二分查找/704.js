/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 * 
    输入: nums = [-1,0,3,5,9,12], target = 9
    输出: 4
    解释: 9 出现在 nums 中并且下标为 4
 */
 var search = function(nums, target) {
    let left = 0, right = nums.length - 1, middle
    while (left <= right) {
        middle = Math.floor((left + right) / 2)
        if (nums[middle] === target) {
            return middle
        } else if (target < nums[middle]) {
            right = middle -1
        } else {
            left = middle + 1
        }
    }
    return -1
};
let res = search(nums = [-1,0,3,5,9,12], target = 9)
console.log(res);