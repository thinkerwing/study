/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 * 
    输入: nums = [1,3,5,6], target = 5
    输出: 2
    输入: nums = [1,3,5,6], target = 2
    输出: 1
    输入: nums = [1,3,5,6], target = 7
    输出: 4
    输入: nums = [1,3,5,6], target = 0
    输出: 0
    右移运算符>>,运算结果正好能对应一个整数的二分之一值，这就正好能代替数学上的除2运算，但是比除2运算要快。
    mid=(left+right)>>1相当于mid=(left+right)/2
 */
var searchInsert = function (nums, target) {
	let n = nums.length
	let left = 0,
		right = n - 1,
		ans = n
	while (left <= right) {
		let mid = ((right - left) >> 1) + left
		if (target <= nums[mid]) {
			ans = mid
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return ans
}
let res = searchInsert((nums = [1, 3, 5, 6]), (target = 5))
console.log(res)
let res2 = searchInsert((nums = [1, 3, 5, 6]), (target = 2))
console.log(res2)
let res3 = searchInsert((nums = [1, 3, 5, 6]), (target = 7))
console.log(res3)
let res4 = searchInsert((nums = [1, 3, 5, 6]), (target = 0))
console.log(res4)
