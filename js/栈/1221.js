/**
 * @param {string} s
 * @return {number}
 * 
输入：s = "RLRRLLRLRL"
输出：4
解释：s 可以分割为 "RL"、"RRLL"、"RL"、"RL" ，每个子字符串中都包含相同数量的 'L' 和 'R' 。

 */
var balancedStringSplit = function (s) {
    let res = 0, total = 0
    for (let b of s) {
        total += b == 'R' ? -1 : 1
        if (total == 0) res += 1
    }
    return res
}
let res = balancedStringSplit(s = "RLRRLLRLRL")
console.log(res);