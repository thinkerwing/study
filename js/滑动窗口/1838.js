/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number} 滑动窗口
 */
var maxFrequency = function (nums, k) {
    nums.sort((a, b) => a - b)
    let res = 1, l = 0, sum = 0
    let n = nums.length
    for (let r = 1; r < n; r++) {
        sum += (nums[r] - nums[r - 1]) * (r - l)
        while (sum > k) {
            sum -= nums[r] - nums[l]
            l += 1
        }
        res = Math.max(res, r - l + 1)
    }
    return res
};
let res = maxFrequency(nums = [1, 2, 4], k = 5)
console.log(res);