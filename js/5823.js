/**
 * @param {string} s
 * @param {number} k
 * @return {number}
输入：s = "iiii", k = 1
输出：36
解释：操作如下：
- 转化："iiii" ➝ "(9)(9)(9)(9)" ➝ "9999" ➝ 9999
- 转换 #1：9999 ➝ 9 + 9 + 9 + 9 ➝ 36
因此，结果整数为 36 。
 */
var getLucky = function (s, k) {
    let arr = []
    let sum = void 0
    /*
    void 是一元运算符,它可以出现在任意类型的操作数之前执行操作数,却忽略操作数的返回值,返回一个 undefined
     */
    for (let i = 0; i < s.length; i++) {
        // console.log(s[i].charCodeAt()-96);
        arr.push(s[i].charCodeAt() - 96)
    }
    let str = arr.join('')
    while (k) {
        let sum = 0
        // console.log(k);
        for (let i = 0; i < str.length; i++) {
            sum += parseInt(str[i])
        }
        str = String(sum)
        k--
    }
    return str
};
let res = getLucky(s = "leetcode", k = 2)
console.log(res);