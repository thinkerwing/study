/**
 * @param {string} time
 * @return {string}
 */
var maximumTime = function (time) {
    let h1 = time[0] !== '?' ? time[0] : (time[1] > 3 ? '1' : '2')
    let h2 = time[1] !== '?' ? time[1] : (h1 === '2' ? '3' : '9')
    let m1 = time[3] !== '?' ? time[3] : '5'
    let m2 = time[4] !== '?' ? time[4] : '9'
    return `${h1}${h2}:${m1}${m2}`
};
let res = maximumTime(time = "2?:?0")
console.log(res);