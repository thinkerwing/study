/**
 * @param {number[]} nums
 * @return {number[]}
 * 集合 s 包含从 1 到 n 的整数。
 * 请你找出重复出现的整数，再找到丢失的整数，将它们以数组的形式返回。
    输入：nums = [1,2,2,4]
    输出：[2,3]
 */
 var findErrorNums = function(nums) {
    console.log(nums);
    let b = []
    let newArr=[]
    let map = new Map()
    for (let i = 0; i <= nums.length; i++) {
        let c = nums[i]
        map.set(c, map.has(c) ? map.get(c) + 1 : 1)
        if(!nums.includes(i) && i> 0)  {
            b.push(i)
        }
    }
    let a =  Array.from(map).map( str => {
        if (str[1] > 1) {
             b.unshift(str[0])
        } 
    })
    return  b
};
var findErrorNums2 = function(nums) {
    let arr = new Array(nums.length + 1).fill(0)
    for (let i = 0; i < nums.length; i++) {
        arr[nums[i]]++
    }
    arr.shift()
    return [arr.indexOf(2) + 1, arr.indexOf(0) + 1]
};
let res = findErrorNums(nums = [1,2,2,4])
let res2 = findErrorNums2(nums = [1,2,2,4])

console.log('res->',  res);
console.log('res2->',  res2);