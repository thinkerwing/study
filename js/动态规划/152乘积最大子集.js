/**
 * @param {number[]} nums
 * @return {number}
 */
 var maxProduct = function (nums) {
    if (nums.length == 0) return 0
    let imax = [...nums], imin = [...nums]
    let ans = imax[0]
    for (let i = 1; i < nums.length; i++) {
        imax[i] = Math.max(imax[i - 1] * nums[i], Math.max(nums[i], imin[i - 1] * nums[i]))
        imin[i] = Math.min(imin[i - 1] * nums[i], Math.min(nums[i], imax[i - 1] * nums[i]))
        ans = Math.max(ans, imax[i])
    }

    return ans
};
let res = maxProduct ([2,3,-2,4])
console.log(res);