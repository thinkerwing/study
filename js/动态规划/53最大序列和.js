var maxSubArray = function (nums) {
	let sum = 0
	let ans = nums[0]
	for (num of nums) {
		if (sum > 0) {
			sum = sum + num
		} else {
			sum = num
		}
		ans = Math.max(sum, ans)
	}
	return ans
}
let res = maxSubArray(nums = [-2,1,-3,4,-1,2,1,-5,4])
console.log(res);
